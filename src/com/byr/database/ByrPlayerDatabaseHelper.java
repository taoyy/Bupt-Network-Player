package com.byr.database;

import java.util.HashMap;
import com.byr.byrplayer.R;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ByrPlayerDatabaseHelper extends SQLiteOpenHelper {
	
	private static boolean IS_ON_CREATE_PROCESSED = false;
	private static final String DATABASE_NAME = "byrplayer.db";
	private static final int DATABASE_VERSION = 8;
	
	//网络收音机表名及表项
	private static final String TABLE_RADIOSET_NAME = "radio";
	public static final String RADIO_ID = "_id";
	public static final String RADIO_SET_NAME = "radio_name";
	public static final String RADIO_SET_DESCR = "radio_descr";
	public static final String RADIO_SET_URL = "radio_url";
	public static final String RADIO_SET_IMG_ID = "radio_image_id";
	
	//网络电视（两张表）表1--分组表
	private static final String TABLE_TVCAT_NAME = "tv_cat";
	public static final String TV_CAT_ID = "_id";
	public static final String TV_CAT_NAME = "tv_cat_name";
	public static final String TV_CAT_DESC = "tv_cat_desc";
	public static final String TV_CAT_IMG_ID = "tv_cat_img_id";
	public static final String TV_CAT_CHILD_NUMBER = "tv_cat_child_number";
	
	//网络电视（两张表）表2--组成员表
	private static final String TABLE_TVSET_NAME = "tv_set";
	public static final String TV_SET_ID = "_id";
	public static final String TV_SET_NAME = "tv_set_name";
	public static final String TV_SET_DESCR = "tv_set_descr";
	public static final String TV_SET_URL = "tv_set_url";
	public static final String TV_SET_IMG_URL = "tv_set_image_id";
	public static final String TV_SET_CAT_NAME = "tv_set_father";
	public static final String TV_SET_CAT_ID = "tv_set_cat_id";

	//推荐表名及表项
	private static final String TABLE_RECOMMEND_NAME = "recommend";
	public static final String RECOMMEND_ID = "_id";
	public static final String RECOMMEND_NAME = "recommend_name";
	public static final String RECOMMEND_DESCR = "recommend_descr";
	public static final String RECOMMEND_URL = "recommend_url";
	public static final String RECOMMEND_IMG_URL = "recommend_image_url";
	public static final String RECOMMEND_TYPE_NAME = "recommend_type";

	
	public ByrPlayerDatabaseHelper(Context context) {	
		// TODO Auto-generated constructor stub
		super(context,DATABASE_NAME,null,DATABASE_VERSION);
		Log.d("databaseinfo","database initing (just creating)..");
	}
	
	public static boolean isOnCreateProcessed() {
		return IS_ON_CREATE_PROCESSED;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		IS_ON_CREATE_PROCESSED = true;
		String sql_byr_radio = "CREATE TABLE " + TABLE_RADIOSET_NAME + "("
				+ RADIO_ID + " integer primary key autoincrement,"
				+ RADIO_SET_NAME + " varchar(100) not null,"
				+ RADIO_SET_DESCR + " varchar(100) not null,"
				+ RADIO_SET_URL + " varchar(512) not null,"
				+ RADIO_SET_IMG_ID + " integer default 0"
				+ ")";
		
		String sql_byrtv_cat = "CREATE TABLE " + TABLE_TVCAT_NAME + "("
				+ TV_CAT_ID + " integer primary key autoincrement,"
				+ TV_CAT_NAME + " varchar(100) not null,"
				+ TV_CAT_DESC + " varchar(100) not null,"
				+ TV_CAT_IMG_ID + " integer default 0,"
				+ TV_CAT_CHILD_NUMBER + " integer default 0" 
				+ ")";
		
		String sql_byrtv_set = "CREATE TABLE " + TABLE_TVSET_NAME + "("
				+ TV_SET_ID + " integer primary key autoincrement,"
				+ TV_SET_NAME + " varchar(100) not null,"
				+ TV_SET_DESCR + " varchar(100) not null,"
				+ TV_SET_URL + " varchar(512) not null,"
				+ TV_SET_IMG_URL + " varchar(512) not null,"
				+ TV_SET_CAT_NAME +" varchar(100) not null,"
				+ TV_SET_CAT_ID + " integer default 0"
				+ ")";
		
		String sql_byr_recommend = "CREATE TABLE " + TABLE_RECOMMEND_NAME + "("
				+ RECOMMEND_ID  + " integer primary key autoincrement,"
				+ RECOMMEND_NAME  + " varchar(100) not null,"
				+ RECOMMEND_DESCR  + " varchar(100) not null,"
				+ RECOMMEND_URL  + " varchar(512) not null,"
				+ RECOMMEND_IMG_URL  + " varchar(512) not null,"
				+ RECOMMEND_TYPE_NAME +" varchar(100) not null"
				+ ")";
		
		try{
			db.execSQL(sql_byr_radio);
			db.execSQL(sql_byrtv_cat);
			db.execSQL(sql_byrtv_set);
			db.execSQL(sql_byr_recommend);
			System.out.println("table create successful");
			Log.d("databaseinfo","table create success");
		}catch(Exception e){
			System.out.println("table create failed");
			Log.d("databaseinfo","table create failed");
		}finally{
			Log.d("databaseinfo","table create end,whether success or not.");
			
		}
		initRadioData(db);
		initTvCatData(db);
		initTvSetData(db);
		initRecommendData(db);
	}
	
	public void initRadioData(SQLiteDatabase db){
			tryToInsertRadioSet("往事并不如烟","china central teletision1","ee1",R.drawable.launcher,db);
	    	tryToInsertRadioSet("CCTV2","china central teletision2","ee2",R.drawable.launcher,db);
	    	tryToInsertRadioSet("CCTV3","china central teletision3","ee3",R.drawable.feng,db);
	}
	
	public void initTvCatData(SQLiteDatabase db){
		tryToInsertTvCat("央视", "nimade",R.drawable.feng,db);
		tryToInsertTvCat("地方卫视", "nimade",R.drawable.feng,db);
	}
	
	public void initTvSetData(SQLiteDatabase db) {
		this.tryToInsertTvSet("ccav1", "this is ccav1", "cccav1", "1", "央视", db);
		this.tryToInsertTvSet("ccav2", "this is ccav2", "cccav2","1", "央视", db);
		this.tryToInsertTvSet("ccav2", "this is ccav3", "cccav2","1", "地方卫视", db);
		this.tryToInsertTvSet("ccav2", "this is ccav4", "cccav2","1", "地方卫视", db);
	}
	
	public void initRecommendData(SQLiteDatabase db){
		this.tryToInsertRecommend("推荐3","测试推荐","aaa","bbb","测试", db);
		this.tryToInsertRecommend("推荐4","测试推荐","aaa","bbb","测试", db);
		this.deleteAllRecommend(db);
		this.tryToInsertRecommend("推荐1","测试推荐","aaa","bbb","测试", db);
		this.tryToInsertRecommend("推荐2","测试推荐","aaa","bbb","测试", db);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		String sql_group = "DROP TABLE IF EXISTS " +TABLE_RADIOSET_NAME;
		db.execSQL(sql_group);
		Log.d("databaseinfo","table updating..");
	}

	public  boolean tryToInsertRadioSet(String name,String desc,String url,int imgid){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql_byrtv = "insert into "+TABLE_RADIOSET_NAME+"( "
				+RADIO_SET_NAME+" , "
				+RADIO_SET_DESCR+" , "
				+RADIO_SET_URL+" , "
				+RADIO_SET_IMG_ID
				+" ) "+"values(?,?,?,?)";
		
		try{
			if(!this.isNameExist(name)){
				db.execSQL(sql_byrtv,new Object[]{name,desc,url,imgid});
				Log.d("databaseinfo","table insert success");
				return true;
			}else{
				this.updateRadioSet(name, desc, url);
				Log.d("databaseinfo","table insert interrupted,value already exist!");
				return false;
			}
			
		}catch(Exception e){
			System.out.println("table insert failed");
			Log.d("databaseinfo","table insert failed");
		}
		return false;
	}
	
	public  boolean tryToInsertRadioSet(String name,String desc,String url,int imgid,SQLiteDatabase db){
		String sql_byrtv = "insert into "+TABLE_RADIOSET_NAME+"( "
				+RADIO_SET_NAME+" , "
				+RADIO_SET_DESCR+" , "
				+RADIO_SET_URL+" , "
				+RADIO_SET_IMG_ID
				+" ) "+"values(?,?,?,?)";
		
		try{
				db.execSQL(sql_byrtv,new Object[]{name,desc,url,imgid});
				Log.d("databaseinfo","table insert success");
				return true;
		}catch(Exception e){
			System.out.println("table insert failed");
			Log.d("databaseinfo","table insert failed");
		}
		return false;
	}
	
	public boolean updateRadioSet(String name,String desc,String url){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql_byrtv = "update "+TABLE_RADIOSET_NAME+" set "
				+RADIO_SET_DESCR+" =?,"
				+RADIO_SET_URL+" =?"
				+" where "+RADIO_SET_NAME+" =?";
		
		try{
			db.execSQL(sql_byrtv,new Object[]{desc,url,name});
			Log.d("databaseinfo","table update success");
			return true;
		}catch(Exception e){
			System.out.println("table update failed");
			Log.d("databaseinfo","table update failed");
		}
		return false;
	}
	
	public HashMap<String,String> getRadioUrl(String name){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql_byrtv = "select * from "+TABLE_RADIOSET_NAME+" where "+RADIO_SET_NAME+" =?";
		try{
			Cursor cursor = db.rawQuery(sql_byrtv, new String[]{name});
			HashMap<String,String> hash = null;
			if(cursor.moveToFirst()){
				Log.d("databaseinfo","finding phone successful"+cursor.getString(cursor.getColumnIndex(RADIO_SET_URL)));
				hash = new  HashMap<String,String>();
				hash.put(RADIO_SET_URL, cursor.getString(cursor.getColumnIndex(RADIO_SET_URL)));
				hash.put(RADIO_ID, cursor.getString(cursor.getColumnIndex(RADIO_ID)));
				hash.put(RADIO_SET_DESCR, cursor.getString(cursor.getColumnIndex(RADIO_SET_DESCR)));
				hash.put(RADIO_SET_IMG_ID, cursor.getString(cursor.getColumnIndex(RADIO_SET_IMG_ID)));
				hash.put(RADIO_SET_NAME, cursor.getString(cursor.getColumnIndex(RADIO_SET_NAME)));
			}else{
				Log.d("databaseinfo","finding  nothing");
			}
			return hash;
		}catch(Exception e){
			Log.d("databaseinfo","finding phone failed");
		}
		return null;
	}
	
	public boolean isNameExist(String name){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql_byrtv = "select * from "+TABLE_RADIOSET_NAME+" where "+RADIO_SET_NAME+" =?";
		try{
			Cursor cursor = db.rawQuery(sql_byrtv, new String[]{name});
			
			if(cursor.moveToFirst()){
				Log.d("databaseinfo","Value already exist,highly recommand not to insert! -- out gate exist");
				//cursor.close();
				return true;
			}else{
				Log.d("databaseinfo","Value doesn't exist,good to insert! -- out gate correct");
				//cursor.close();
				return false;
			}
			
		}catch(Exception e){
			Log.d("databaseinfo","Maybe table doesn'd exist,do not insert! -- out gate wrong");
			return true;
		}
	}
	
	public boolean deleteRadioSet(String name){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql_byrtv = "delete from "+TABLE_RADIOSET_NAME+" where "+RADIO_SET_NAME+" = ?";
		try{
			db.execSQL(sql_byrtv,new String[]{name});
			 Log.d("databaseinfo","delete phone success");
			return true;
		}catch(Exception e){
			Log.d("databaseinfo","delete phone failed");
		}
		return false;
	}
	
	public boolean deleteAllRadioSet(){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql_byrtv = "delete from "+TABLE_RADIOSET_NAME;
		try{
			db.execSQL(sql_byrtv);
			 Log.d("databaseinfo","delete all from radio set success");
			return true;
		}catch(Exception e){
			Log.d("databaseinfo","delete all from radio set failed");
		}
		return false;
	}
	
	public Cursor getAllRadioSets(){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql_byrtv = "select * from "+TABLE_RADIOSET_NAME;
		Cursor cursor = db.rawQuery(sql_byrtv, null);
		return cursor; 
	}
	
	
	//---------------------------------------the next part is the operation for the Bupt tv------------------------------------
	//-----------------------------------------------下一部分是北邮人电视的数据库操作部分---------------------------------------------------

	public  boolean tryToInsertTvCat(String name,String desc,int imgid,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv_cat = "insert into "+TABLE_TVCAT_NAME +"( "
				+TV_CAT_NAME +" , "
				+TV_CAT_DESC +" , "
				+TV_CAT_IMG_ID 
				+" ) "+"values(?,?,?)";
		
		try{
				if(database != null || !isNameExistInTvCat(name,null))
					db.execSQL(sql_byrtv_cat,new Object[]{name,desc,imgid});
				Log.d("databaseinfo","table insert success");
				return true;
		}catch(Exception e){
			System.out.println("table insert failed");
			Log.d("databaseinfo","table insert failed");
		}
		return false;
	}
	
	public boolean updateTvCat(String name,String desc,int imgid,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv = "update "+TABLE_TVCAT_NAME+" set "
				+TV_CAT_DESC+" =?,"
				+TV_CAT_IMG_ID+" =?"
				+" where "+TV_CAT_NAME+" =?";
		
		try{
			db.execSQL(sql_byrtv,new Object[]{desc,imgid,name});
			Log.d("databaseinfo","table update success");
			return true;
		}catch(Exception e){
			System.out.println("table update failed");
			Log.d("databaseinfo","table update failed");
		}
		return false;
	}
	
	public boolean isNameExistInTvCat(String name,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv = "select * from "+TABLE_TVCAT_NAME+" where "+TV_CAT_NAME+" =?";
		try{
			Cursor cursor = db.rawQuery(sql_byrtv, new String[]{name});
			if(cursor.moveToFirst()){
				Log.d("databaseinfo","TVcat--Value already exist,highly recommand not to insert! -- out gate exist");
				//cursor.close();
				return true;
			}else{
				Log.d("databaseinfo","TVcat--Value doesn't exist,good to insert! -- out gate correct");
				//cursor.close();
				return false;
			}
		}catch(Exception e){
			Log.d("databaseinfo","TVcat--Maybe table doesn'd exist,do not insert! -- out gate wrong");
			return true;
		}
	}
	
	public HashMap<String,String> getTvCatInfo(String name,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv = "select * from "+TABLE_TVCAT_NAME+" where "+TV_CAT_NAME+" =?";
		try{
			Cursor cursor = db.rawQuery(sql_byrtv, new String[]{name});
			HashMap<String,String> hash = null;
			if(cursor.moveToFirst()){
				Log.d("databaseinfo","finding phone successful"+cursor.getString(cursor.getColumnIndex(TV_CAT_DESC)));
				hash = new  HashMap<String,String>();
				hash.put(TV_CAT_ID , cursor.getString(cursor.getColumnIndex(TV_CAT_ID )));
				hash.put(TV_CAT_NAME , cursor.getString(cursor.getColumnIndex(TV_CAT_NAME)));
				hash.put(TV_CAT_DESC , cursor.getString(cursor.getColumnIndex(TV_CAT_DESC)));
				hash.put(TV_CAT_IMG_ID , cursor.getString(cursor.getColumnIndex(TV_CAT_IMG_ID)));
				hash.put(TV_CAT_CHILD_NUMBER , cursor.getString(cursor.getColumnIndex(TV_CAT_CHILD_NUMBER)));
			}else{
				Log.d("databaseinfo","finding  nothing");
			}
			return hash;
		}catch(Exception e){
			Log.d("databaseinfo","finding phone failed");
		}
		return null;
	}
	
	public boolean deleteTvCat(String name,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtvcat = "delete from "+TABLE_TVCAT_NAME+" where "+TV_CAT_NAME+" = ?";
		String sql_byrtvset = "delete from "+TABLE_TVSET_NAME +" where "+TV_SET_CAT_NAME +" = ?";
		try{
			db.execSQL(sql_byrtvcat,new String[]{name});
			db.execSQL(sql_byrtvset,new String[]{name});
			 Log.d("databaseinfo","TVcat--delete phone success");
			return true;
		}catch(Exception e){
			Log.d("databaseinfo","TVcat--delete phone failed");
		}
		return false;
	}
	
	public Cursor getAllTvCats(){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql_byrtv = "select * from "+TABLE_TVCAT_NAME;
		Cursor cursor = db.rawQuery(sql_byrtv, null);
		return cursor; 
	}
	//-----------------------------the next part is for the individual tv set of the buptTV----------------------------------
	//-------------------------------------下一部分是北邮人tv电视频道数据库的操作部分------------------------------------------------------
	
	public  boolean tryToInsertTvSet(String name,String desc,String url,String imgurl,String cataloguename,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv_cat = "insert into "+TABLE_TVSET_NAME +"( "
				+TV_SET_NAME +" , "
				+TV_SET_DESCR +" , "
				+TV_SET_URL +" , "
				+TV_SET_IMG_URL +" , "
				+TV_SET_CAT_NAME  
				+" ) "+"values(?,?,?,?,?)";
		
		try{
				if((database != null || !isNameExistInTvSet(name,null)) && isNameExistInTvCat(cataloguename,database))
					db.execSQL(sql_byrtv_cat,new Object[]{name,desc,url,imgurl,cataloguename});
				else 
					throw new Exception();
				Log.d("databaseinfo","TVset--table insert success");
				return true;
		}catch(Exception e){
			System.out.println("TVset--table insert failed");
			Log.d("databaseinfo","TVset--table insert failed");
		}
		return false;
	}
	
	public boolean updateTvSet(String name,String desc,String url,String imgurl,String cataloguename,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv = "update "+TABLE_TVSET_NAME +" set "
				+TV_SET_DESCR + " =?,"
				+TV_SET_URL + " =?,"
				+TV_SET_IMG_URL +" =?,"
				+TV_SET_CAT_NAME +" =?"
				+" where "+TV_SET_NAME+" =?";
		
		try{
			db.execSQL(sql_byrtv,new Object[]{desc,url,imgurl,cataloguename,name});
			Log.d("databaseinfo","table update success");
			return true;
		}catch(Exception e){
			System.out.println("table update failed");
			Log.d("databaseinfo","table update failed");
		}
		return false;
	}
	
	public boolean isNameExistInTvSet(String name,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv = "select * from "+TABLE_TVSET_NAME +" where "+TV_SET_NAME +" =?";
		try{
			Cursor cursor = db.rawQuery(sql_byrtv, new String[]{name});
			if(cursor.moveToFirst()){
				Log.d("databaseinfo","TVset-Value already exist,highly recommand not to insert! -- out gate exist");
				//cursor.close();
				return true;
			}else{
				Log.d("databaseinfo","TVset-Value doesn't exist,good to insert! -- out gate correct");
				//cursor.close();
				return false;
			}
		}catch(Exception e){
			Log.d("databaseinfo","Maybe table doesn'd exist,do not insert! -- out gate wrong");
			return true;
		}
	}
	
	public HashMap<String,String> getTvSetInfo(String name,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv = "select * from "+TABLE_TVSET_NAME +" where "+TV_SET_NAME +" =?";
		try{
			Cursor cursor = db.rawQuery(sql_byrtv, new String[]{name});
			HashMap<String,String> hash = null;
			if(cursor.moveToFirst()){
				Log.d("databaseinfo","finding phone successful"+cursor.getString(cursor.getColumnIndex(TV_SET_DESCR)));
				hash = new  HashMap<String,String>();
				hash.put(TV_SET_ID, cursor.getString(cursor.getColumnIndex(TV_SET_ID)));
				hash.put(TV_SET_NAME , cursor.getString(cursor.getColumnIndex(TV_SET_NAME )));
				hash.put(TV_SET_DESCR , cursor.getString(cursor.getColumnIndex(TV_SET_DESCR )));
				hash.put(TV_SET_URL , cursor.getString(cursor.getColumnIndex(TV_SET_URL )));
				hash.put(TV_SET_IMG_URL , cursor.getString(cursor.getColumnIndex(TV_SET_IMG_URL )));
				hash.put(TV_SET_CAT_NAME , cursor.getString(cursor.getColumnIndex(TV_SET_CAT_NAME )));
				hash.put(TV_SET_CAT_ID , cursor.getString(cursor.getColumnIndex(TV_SET_CAT_ID )));
				
			}else{
				Log.d("databaseinfo","finding  nothing");
			}
			return hash;
		}catch(Exception e){
			Log.d("databaseinfo","finding phone failed");
		}
		return null;
	}
	
	public boolean deleteTvSet(String name,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv = "delete from "+TABLE_TVSET_NAME +" where "+TV_SET_NAME +" = ?";
		try{
			db.execSQL(sql_byrtv,new String[]{name});
			 Log.d("databaseinfo","delete phone success");
			return true;
		}catch(Exception e){
			Log.d("databaseinfo","delete phone failed");
		}
		return false;
	}
	
	public boolean deleteAllTvSet(){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql_byrtv = "delete from "+TABLE_TVSET_NAME;
		try{
			db.execSQL(sql_byrtv);
			 Log.d("databaseinfo","delete all from tv set success");
			return true;
		}catch(Exception e){
			Log.d("databaseinfo","delete all from tv set failed");
		}
		return false;
	}
	
	public Cursor getTvSets(String parentname){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql_byrtv = "select * from "+TABLE_TVSET_NAME +" where "+TV_SET_CAT_NAME +" = ?";
		Cursor cursor = db.rawQuery(sql_byrtv,new String[]{parentname});
		return cursor; 
	}
	
	public Cursor getAllTvSets(){
		SQLiteDatabase db = this.getReadableDatabase();
		//String sql_byrtv = "select top 10 *, NewID() as random from "+TABLE_TVSET_NAME+"order by random ";
		String sql_byrtv = "select * from "+TABLE_TVSET_NAME;
		Cursor cursor = db.rawQuery(sql_byrtv, null);
		return cursor; 
	}
	
	//----------------------------------------------------------下面的部分是北邮人推荐节目的数据库部分-------------------------------------------
	
	public  boolean tryToInsertRecommend(String name,String desc,String url,String imgurl,String cataloguename,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv_cat = "insert into "+TABLE_RECOMMEND_NAME +"( "
				+RECOMMEND_NAME +" , "
				+RECOMMEND_DESCR +" , "
				+RECOMMEND_URL +" , "
				+RECOMMEND_IMG_URL +" , "
				+RECOMMEND_TYPE_NAME  
				+" ) "+"values(?,?,?,?,?)";
		
		try{
				if((database != null || !isNameExistInRecommendSet(name,null)))
					db.execSQL(sql_byrtv_cat,new Object[]{name,desc,url,imgurl,cataloguename});
				else 
					throw new Exception();
				Log.d("databaseinfo","Recommend--table insert success");
				return true;
		}catch(Exception e){
			System.out.println("Recommend--table insert failed");
			Log.d("databaseinfo","Recommend--table insert failed");
		}
		return false;
	}

	public boolean updateRecommend(String name,String desc,String url,String imgurl,String cataloguename,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv = "update "+TABLE_RECOMMEND_NAME +" set "
				+RECOMMEND_DESCR + " =?,"
				+RECOMMEND_URL + " =?,"
				+RECOMMEND_IMG_URL +" =?,"
				+RECOMMEND_TYPE_NAME +" =?"
				+" where "+RECOMMEND_NAME + " =?";
		
		try{
			db.execSQL(sql_byrtv,new Object[]{desc,url,imgurl,cataloguename,name});
			Log.d("databaseinfo","table update success");
			return true;
		}catch(Exception e){
			System.out.println("table update failed");
			Log.d("databaseinfo","table update failed");
		}
		return false;
	}
	
	public boolean isNameExistInRecommendSet(String name,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv = "select * from "+TABLE_RECOMMEND_NAME  +" where "+RECOMMEND_NAME +" =?";
		try{
			Cursor cursor = db.rawQuery(sql_byrtv, new String[]{name});
			if(cursor.moveToFirst()){
				Log.d("databaseinfo","Recommend already exist,highly recommand not to insert! -- out gate exist");
				//cursor.close();
				return true;
			}else{
				Log.d("databaseinfo","Recommend-Value doesn't exist,good to insert! -- out gate correct");
				//cursor.close();
				return false;
			}
		}catch(Exception e){
			Log.d("databaseinfo","Recommend - Maybe table doesn'd exist,do not insert! -- out gate wrong");
			return true;
		}
	}
	
	public boolean deleteRecommend(String name,SQLiteDatabase database){
		@SuppressWarnings("resource")
		SQLiteDatabase db = (database == null)?this.getReadableDatabase():database;
		String sql_byrtv = "delete from "+TABLE_RECOMMEND_NAME +" where "+RECOMMEND_NAME +" = ?";
		try{
			db.execSQL(sql_byrtv,new String[]{name});
			 Log.d("databaseinfo","recommend delete success");
			return true;
		}catch(Exception e){
			Log.d("databaseinfo","recommend delete failed");
		}
		return false;
	}
	
	public boolean deleteAllRecommend(SQLiteDatabase database){
		SQLiteDatabase db = (database==null)?this.getReadableDatabase():database;
		String sql_byrtv = "delete from "+TABLE_RECOMMEND_NAME;
		try{
			db.execSQL(sql_byrtv);
			 Log.d("databaseinfo","delete all from tv set success");
			 //db.close();
			return true;
		}catch(Exception e){
			//db.close();
			Log.d("databaseinfo","delete all from tv set failed");
		}
		return false;
	}
	
	public Cursor getAllRecommend(){
		SQLiteDatabase db = this.getReadableDatabase();
		String sql_byrtv = "select * from "+TABLE_RECOMMEND_NAME;
		Cursor cursor = db.rawQuery(sql_byrtv, null);
		return cursor; 
	}
}
