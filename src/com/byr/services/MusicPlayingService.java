/**
 * 
 */
package com.byr.services;

import com.byr.byrplayer.RadioFragment;
import com.byr.tools.VoiceManageListener;
import com.byr.tools.VoiceManager;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

/**
 * @author Administrator
 * 这个类是一个用于柏刚mms流或者其他动态，静态音频流的类，这个类以一个service的形式存在。
 */
public class MusicPlayingService extends Service {
	VoiceManager mamager;
	public class MyBinder extends Binder implements IService {
		
		@Override
		public void play(String url) {
			// TODO Auto-generated method stub
			VoiceManager.getInstance().playVoiceAsync(url);
		}

		@Override
		public void pause() {
			// TODO Auto-generated method stub
			VoiceManager.getInstance().pauseVoice();
		}

		@Override
		public void resume() {
			// TODO Auto-generated method stub
			VoiceManager.getInstance().resume();
		}

		@Override
		public void reset() {
			// TODO Auto-generated method stub
			VoiceManager.getInstance().reset();
		}

		@Override
		public int getState() {
			// TODO Auto-generated method stub
			return VoiceManager.getInstance().getState();
		}

		@Override
		public void setVoiceManageListener(VoiceManageListener listener) {
			// TODO Auto-generated method stub
			 VoiceManager.getInstance().setVoiceManageListener(listener);
		}

		@Override
		public void destroyMedia() {
			// TODO Auto-generated method stub
			VoiceManager.getInstance().destroy();
		}
		
	}
	
	@Override  	
    public IBinder onBind(Intent intent) {  
		return new MyBinder();
    }  
  
    @Override  
    public void onCreate() {  
    	super.onCreate();
    	mamager = VoiceManager.getInstance();
    }  
  
    @Override  
    public void onDestroy() {  
    	super.onDestroy();
    	VoiceManager.getInstance().destroy();
    	RadioFragment.mNotificationManager.cancel(21394213);
    	Log.d("byrplayerService","service connection destroyed");
    }  
  
    @Override  
    public void onStart(Intent intent, int startId) {  
    	super.onStart(intent, startId);
    }

}
