package com.byr.services;

import com.byr.tools.VoiceManageListener;

public interface IService {
	public void play(String url);
	public void pause();
	public void resume();
	public void reset();
	public int getState();
	/**
	 * �����ṩ��MediaPlayer
	 */
	public void destroyMedia();
	public void setVoiceManageListener(VoiceManageListener listener);
}
