package com.byr.byrplayer;

import com.byr.network.ConnectionServer;
import com.byr.tools.RadioAdapter;
import com.byr.tools.VoiceManageListener;
import com.byr.tools.VoiceManager;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import io.vov.vitamio.widget.MediaController;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ValidFragment")
public class RadioFragment extends Fragment{
	boolean DEBUG = false;
	MainpageActivity parent;
	public RadioAdapter radioAdapter;
	public ListView list;
	@SuppressLint("ValidFragment")
	private PullToRefreshListView mPullRefreshListView;
	View fv;
	private ImageView imagepause;
	private Handler handler = null;
	private RadioFragment that;

	
	/**
	 * 按钮旋转模块和播放模块依赖
	 */
	private enum states{STATE_INITED,STATE_PREPARING,STATE_PAUSED,STATE_PLAYING};
	private states playerstate = states.STATE_INITED;
	private Thread rotatingThread = null;
	private int degree = 0;
	private Bitmap bmp;
	private Bitmap playmap;
	private Bitmap pausemap;
	private boolean rotate = false;
	
	/**
	 * 下面这段是音乐播放的service需要的依赖
	 */
	public static NotificationManager mNotificationManager;  
	public static int currentIntent = 0;
	String currentPlayingName;
	String currentPlayingDes;
	
	public RadioFragment(MainpageActivity m){
		parent = m;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		that =this;
		
		/**
		 * 下面这段是音乐播放的service需要的依赖
		 */
		mNotificationManager = (NotificationManager) MainpageActivity.mcontext.getSystemService(Context.NOTIFICATION_SERVICE);
		
		/**
		 * 下面是程序主体部分依赖
		 */
		MediaController mediaController = new MediaController(parent);
		parent.addContentView(mediaController, new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		handler = new Handler() {  
            @Override  
            public void handleMessage(Message msg) {  
                if(msg.what == -1){

                }else if (msg.what == 1) {
                	if(DEBUG)showToask("同步切换至暂停");
                	if(rotatingThread != null && rotatingThread.isAlive()){
                		rotate = false;
                		degree = 0;
                	}
                	//((ImageView)fv.findViewById(R.id.operate)).setBackground(parent.getResources().getDrawable(R.drawable.pause));
                	imagepause.setImageBitmap(pausemap);  
                }else if(msg.what == 2){
                	if(DEBUG)showToask("同步切换至播放");
                	if(rotatingThread != null && rotatingThread.isAlive()){
                		rotate = false;
                		degree = 0;
                	}
                	//((ImageView)fv.findViewById(R.id.operate)).setBackground(parent.getResources().getDrawable(R.drawable.play));
                	imagepause.setImageBitmap(playmap);
                }else if(msg.what == 3){
                	//创建ProgressDialog对象
                	rotate = true;
                	if(rotatingThread == null){
	                	rotatingThread = new Thread(){
	                		private Message mesg;
	
							public void run(){
								degree = 0;
	                			while(true){
	                				while(!rotate){
										try {
											sleep(10);
										} catch (InterruptedException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
									}
	                				degree++;
		                			mesg = new Message();
		            				mesg.what = 5;
		            				that.handler.sendMessage(mesg);
		                			try {
										Thread.sleep(4);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
	                			}
	                		}
	                	};
	                	rotatingThread.start();
                	}
			       

                }else if(msg.what == 4){

                }else if(msg.what == 5){
                	startRotate(degree);
                }
            }
        };
        
		fv = inflater.inflate(R.layout.fragment_list, null);
		imagepause = (ImageView)fv.findViewById(R.id.operate);
		imagepause.setScaleType(ScaleType.CENTER);
		playmap = bmp= BitmapFactory.decodeResource  
		        (getResources(), R.drawable.play);
		pausemap = bmp= BitmapFactory.decodeResource  
		        (getResources(), R.drawable.pause);
		bmp = playmap;
		Bitmap turnBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), new Matrix(), true);  
        imagepause.setImageBitmap(turnBmp);  
		mPullRefreshListView = (PullToRefreshListView)fv.findViewById(R.id.ListView01);
		// Set a listener to be invoked when the list should be refreshed.
				mPullRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(PullToRefreshBase<ListView> refreshView) {
						String label = DateUtils.formatDateTime(parent.getApplicationContext(), System.currentTimeMillis(),
								DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

						// Do work to refresh the list here.
						new GetDataTask().execute();
					}
				});

				// Add an end-of-list listener
				mPullRefreshListView.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

					@Override
					public void onLastItemVisible() {
						
					}
				});

		list = mPullRefreshListView.getRefreshableView(); 
		radioAdapter = new RadioAdapter(parent,MainpageActivity.databaseHelper.getAllRadioSets(),true);
        //添加并且显示
        list.setAdapter(radioAdapter);
        //添加点击
        list.setOnItemClickListener(new OnItemClickListener(){
        	@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2,long arg3) {
        		TextView urlText = (TextView)view.findViewById(R.id.url);
				final String url =urlText.getText().toString();
				String name = ((TextView)view.findViewById(R.id.ItemTitle)).getText().toString();
				String desc = ((TextView)view.findViewById(R.id.ItemText)).getText().toString();
				currentPlayingName = name;
				currentPlayingDes = desc;
				((TextView)fv.findViewById(R.id.radioname)).setText(name.toCharArray(), 0, name.length());
				((TextView)fv.findViewById(R.id.radiodesc)).setText(desc.toCharArray(), 0, desc.length());
				Log.d("list",url);
				//VoiceManager.getInstance().playVoiceAsync(url);
				MainpageActivity.iService.play(url);
			}
        });
        ((ImageView)fv.findViewById(R.id.operate)).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View view) {
				if(MainpageActivity.iService.getState() == VoiceManager.STATE_PAUSED)
					MainpageActivity.iService.resume();
				else{
					MainpageActivity.iService.pause();
				}
			}
        	
        });
        
        VoiceManager.getInstance().setVoiceManageListener(new VoiceManageListener() {
			
			@Override
			public void OnReset() {
				// TODO Auto-generated method stub
				buttonReset();
				Log.d("byrmediaplay","OnReset");
			}
			
			@Override
			public void OnPreparing() {
				// TODO Auto-generated method stub
				buttonPreparing();
				Log.d("byrmediaplay","OnPreparing");
			}
			
			@Override
			public void OnPrepared() {
				// TODO Auto-generated method stub
				buttonPlaying();
				Log.d("byrmediaplay","OnPrepared");
			}
			
			@Override
			public void OnPause() {
				// TODO Auto-generated method stub
				buttonPaused();
				Log.d("byrmediaplay","OnPause");
				setMsgPause(currentPlayingName,currentPlayingDes);
			}
			
			@Override
			public void OnError(Exception e) {
				// TODO Auto-generated method stub
				buttonReset();
				Log.d("byrmediaplay","OnError");
			}

			@Override
			public void OnResume() {
				// TODO Auto-generated method stub
				buttonPlaying();
				Log.d("byrmediaplay","OnResume");
				setMsgPlaying(currentPlayingName,currentPlayingDes);
			}

			@Override
			public void OnComplete() {
				// TODO Auto-generated method stub
				buttonReset();
				Log.d("byrmediaplay","OnComplete");
				setMsgPause(currentPlayingName,currentPlayingDes);
			}

			@Override
			public void OnPlay() {
				// TODO Auto-generated method stub
				buttonPlaying();
				Log.d("byrmediaplay","OnPlay");
				setMsgPlaying(currentPlayingName,currentPlayingDes);
			}
		});
        //setMsgNotification();
		return fv;
	}
	
	private class GetDataTask extends AsyncTask<Void, Void, String[]> {

		@Override
		protected String[] doInBackground(Void... params) {
			// Simulates a background job.
			try {
				MainpageActivity.radiorefreshed = false;
				new Thread(){
					public void run(){
						ConnectionServer.requireRadioUpdate();
					}
				}.start();
				for(int i = 0;i < 40 && MainpageActivity.radiorefreshed == false;i++)
					Thread.sleep(100);
				if(MainpageActivity.radiorefreshed){
					Message msg = new Message();  
    	            msg.what = -1;  
    	            MainpageActivity.handler.sendMessage(msg);  
				}else{
					Message msg = new Message();  
    	            msg.what = -2;  
    	            MainpageActivity.handler.sendMessage(msg); 
				}
			} catch (InterruptedException e) {
			}
			return null;
		}

		@Override
		protected void onPostExecute(String[] result) {
			// Call onRefreshComplete when the list has been refreshed.
			mPullRefreshListView.onRefreshComplete();

			super.onPostExecute(result);
		}
	}
	
	void showToask(String hint) {
		Toast toast = Toast.makeText(this.getActivity(), hint, Toast.LENGTH_SHORT);
		toast.show();
	}
	
	
	/**
	 * 下面是旋转按钮模块，这个模块之所以不从这个Fragmeng中移出是因为想要操作ImageView必须在创建他的进程中完成
	 */
	public void buttonPreparing(){
		if(playerstate == states.STATE_INITED){
			bmp = playmap;
			Message mesg = new Message();
			mesg.what = 3;
			that.handler.sendMessage(mesg);
			playerstate = states.STATE_PREPARING;
		}else if(playerstate == states.STATE_PAUSED || playerstate == states.STATE_PLAYING){
			bmp = playmap;
			Message mesg = new Message();
			mesg.what = 3;
			that.handler.sendMessage(mesg);
			playerstate = states.STATE_PREPARING;
		}
	}

	public void buttonPlaying(){
		if(playerstate == states.STATE_PAUSED || playerstate == states.STATE_PREPARING || playerstate == states.STATE_INITED){
			bmp = pausemap;
			Message mesg = new Message();
			mesg.what = 3;
			that.handler.sendMessage(mesg);
			playerstate = states.STATE_PLAYING;
		}
	}
	
	public void buttonPaused(){
		if(playerstate == states.STATE_PLAYING){
			bmp = playmap;
			Message mesg = new Message();
			mesg.what = 1;
			that.handler.sendMessage(mesg);
			playerstate = states.STATE_PAUSED;
		}
	}

	public void buttonReset(){
		bmp = playmap;
		Message mesg = new Message();
		mesg.what = 1;
		that.handler.sendMessage(mesg);
		playerstate = states.STATE_INITED;
	}
	
	private void startRotate(int degree){
		Matrix mt = new Matrix();  
        mt.postRotate(degree);  
        Bitmap turnBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mt, true);  
        imagepause.setImageBitmap(turnBmp);  
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		/*if(MainpageActivity.iService.getState() == VoiceManager.STATE_INITED){
			buttonReset();
		}else if(MainpageActivity.iService.getState() == VoiceManager.STATE_PAUSED){
			buttonPaused();
		}else if(MainpageActivity.iService.getState() == VoiceManager.STATE_PLAYING){
			buttonPlaying();
		}else if(MainpageActivity.iService.getState() == VoiceManager.STATE_PREPARING){
			buttonPreparing();
		}*/
	}
	
	private void setMsgNotification() {  
        int icon = R.drawable.launcher;  
        CharSequence tickerText = "";  
        long when = System.currentTimeMillis();  
        Notification mNotification = new Notification(icon, tickerText, when);  
  
        // 放置在"正在运行"栏目中  
        mNotification.flags = Notification.FLAG_ONGOING_EVENT;  
        RemoteViews contentView = new RemoteViews(parent.getPackageName(),  
                R.layout.layout_mediaplayerservice);  
        contentView.setTextViewText(R.id.ItemTitle, "无歌曲");  
        contentView.setTextViewText(R.id.ItemText, "music your life");  
        // 指定个性化视图  
        
        
        /**
         * 下面是监听
         */
        final String STATUS_BAR_COVER_CLICK_ACTION="download" + currentIntent;
        BroadcastReceiver onClickReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
	        if (intent.getAction().equals(STATUS_BAR_COVER_CLICK_ACTION)) {
	        //在这里处理点击事件
	        	Log.d("byrplayerService","received message INIting");
	        	if(MainpageActivity.iService.getState() == VoiceManager.STATE_PAUSED)
					MainpageActivity.iService.resume();
				else{
					MainpageActivity.iService.pause();
				}
	        //取消通知栏
	        }
        }};
        IntentFilter filter = new IntentFilter();
        filter.addAction(STATUS_BAR_COVER_CLICK_ACTION);
        MainpageActivity.mcontext.registerReceiver(onClickReceiver, filter);
        Intent buttonIntent = new Intent(STATUS_BAR_COVER_CLICK_ACTION);
        PendingIntent pendButtonIntent = PendingIntent.getBroadcast(MainpageActivity.mcontext, 0, buttonIntent, 0);
        contentView.setOnClickPendingIntent(R.id.playimage, pendButtonIntent);  
        //R.id.trackname为你要监听按钮的id
        // mRemoteViews.setOnClickPendingIntent(R.id.trackname, pendButtonIntent);

        
        
        /*Intent intent = new Intent(this, FriendListActivity.class);  
        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0,  
                intent, PendingIntent.FLAG_UPDATE_CURRENT);  
        // 指定内容意图  
        mNotification.contentIntent = contentIntent;  */
        mNotification.contentView = contentView;  
        mNotificationManager.notify(21394213, mNotification);  
    }  
	
	private void setMsgPlaying(String name,String des) {  
        int icon = R.drawable.launcher;  
        CharSequence tickerText = "";  
        long when = System.currentTimeMillis();  
        Notification mNotification = new Notification(icon, tickerText, when);  
  
        // 放置在"正在运行"栏目中  
        mNotification.flags = Notification.FLAG_ONGOING_EVENT;  
        RemoteViews contentView = new RemoteViews(parent.getPackageName(),  
                R.layout.layout_mediaplayerservice);  
        contentView.setTextViewText(R.id.ItemTitle, name);  
        contentView.setTextViewText(R.id.ItemText, des);
        contentView.setImageViewResource(R.id.playimage, R.drawable.pause);  
        // 指定个性化视图  
        
        
        /**
         * 下面是监听
         */
        final String STATUS_BAR_COVER_CLICK_ACTION="download" + currentIntent;
        BroadcastReceiver onClickReceiver = new BroadcastReceiver() {
        private boolean flag = false;
        @Override
        public void onReceive(Context context, Intent intent) {
	        if (intent.getAction().equals(STATUS_BAR_COVER_CLICK_ACTION)) {
	        //在这里处理点击事件
	        	Log.d("byrplayerService","received message Playing");
	        	if(MainpageActivity.iService.getState() == VoiceManager.STATE_PAUSED){
					//MainpageActivity.iService.resume();
	        	}else if(MainpageActivity.iService.getState() == VoiceManager.STATE_PLAYING){
					MainpageActivity.iService.pause();
				}
	        //取消通知栏
	        }
        }};
        IntentFilter filter = new IntentFilter();
        filter.addAction(STATUS_BAR_COVER_CLICK_ACTION);
        MainpageActivity.mcontext.registerReceiver(onClickReceiver, filter);
        Intent buttonIntent = new Intent(STATUS_BAR_COVER_CLICK_ACTION);
        PendingIntent pendButtonIntent = PendingIntent.getBroadcast(MainpageActivity.mcontext, currentIntent, buttonIntent, 0);
        contentView.setOnClickPendingIntent(R.id.playimage, pendButtonIntent);  
        //R.id.trackname为你要监听按钮的id
        // mRemoteViews.setOnClickPendingIntent(R.id.trackname, pendButtonIntent);
        BroadcastReceiver onCloseReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            	Log.d("byrplayerService","received message close");
    	        if (intent.getAction().equals(STATUS_BAR_COVER_CLICK_ACTION+"close")) {
    	        	//取消通知栏
    	        	mNotificationManager.cancel(21394213);
    	        }
            }};
        IntentFilter filterclose = new IntentFilter();
        filterclose.addAction(STATUS_BAR_COVER_CLICK_ACTION+"close");
        MainpageActivity.mcontext.registerReceiver(onCloseReceiver, filterclose);
        Intent closeIntent = new Intent(STATUS_BAR_COVER_CLICK_ACTION+"close");
        PendingIntent pendCloseIntent = PendingIntent.getBroadcast(MainpageActivity.mcontext, currentIntent++, closeIntent, 0);
        contentView.setOnClickPendingIntent(R.id.close, pendCloseIntent);  

        
        /*Intent intent = new Intent(this, FriendListActivity.class);  
        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0,  
                intent, PendingIntent.FLAG_UPDATE_CURRENT);  
        // 指定内容意图  
        mNotification.contentIntent = contentIntent;  */
        mNotification.contentView = contentView;  
        mNotificationManager.notify(21394213, mNotification);  
    }  
	
	private void setMsgPause(String name,String des) {  
        int icon = R.drawable.launcher;  
        CharSequence tickerText = "";  
        long when = System.currentTimeMillis();  
        Notification mNotification = new Notification(icon, tickerText, when);  
  
        // 放置在"正在运行"栏目中  
        mNotification.flags = Notification.FLAG_ONGOING_EVENT;  
        RemoteViews contentView = new RemoteViews(parent.getPackageName(),  
                R.layout.layout_mediaplayerservice);  
        contentView.setTextViewText(R.id.ItemTitle, name);  
        contentView.setTextViewText(R.id.ItemText, des);
        contentView.setImageViewResource(R.id.playimage, R.drawable.play);  
        // 指定个性化视图  
        
        
        /**
         * 下面是监听
         */
        final String STATUS_BAR_COVER_CLICK_ACTION="download" + currentIntent;
        BroadcastReceiver onClickReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
		        if (intent.getAction().equals(STATUS_BAR_COVER_CLICK_ACTION)) {
		        //在这里处理点击事件
		        	Log.d("byrplayerService","received message Pause");
		        	if(MainpageActivity.iService.getState() == VoiceManager.STATE_PAUSED){
						MainpageActivity.iService.resume();
		        	}else if(MainpageActivity.iService.getState() == VoiceManager.STATE_PLAYING){
						//MainpageActivity.iService.pause();
					}
		        //取消通知栏
		        }
        }};
        IntentFilter filter = new IntentFilter();
        filter.addAction(STATUS_BAR_COVER_CLICK_ACTION);
        MainpageActivity.mcontext.registerReceiver(onClickReceiver, filter);
        Intent buttonIntent = new Intent(STATUS_BAR_COVER_CLICK_ACTION);
        PendingIntent pendButtonIntent = PendingIntent.getBroadcast(MainpageActivity.mcontext, currentIntent, buttonIntent, 0);
        contentView.setOnClickPendingIntent(R.id.playimage, pendButtonIntent);  
        //R.id.trackname为你要监听按钮的id
        // mRemoteViews.setOnClickPendingIntent(R.id.trackname, pendButtonIntent);
        BroadcastReceiver onCloseReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            	Log.d("byrplayerService","received message close");
    	        if (intent.getAction().equals(STATUS_BAR_COVER_CLICK_ACTION+"close")) {
    	        	//取消通知栏
    	        	mNotificationManager.cancel(21394213);
    	        }
            }};
        IntentFilter filterclose = new IntentFilter();
        filterclose.addAction(STATUS_BAR_COVER_CLICK_ACTION+"close");
        MainpageActivity.mcontext.registerReceiver(onCloseReceiver, filterclose);
        Intent closeIntent = new Intent(STATUS_BAR_COVER_CLICK_ACTION+"close");
        PendingIntent pendCloseIntent = PendingIntent.getBroadcast(MainpageActivity.mcontext, currentIntent++, closeIntent, 0);
        contentView.setOnClickPendingIntent(R.id.close, pendCloseIntent);  
        
        
        /*Intent intent = new Intent(this, FriendListActivity.class);  
        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0,  
                intent, PendingIntent.FLAG_UPDATE_CURRENT);  
        // 指定内容意图  
        mNotification.contentIntent = contentIntent;  */
        mNotification.contentView = contentView;  
        mNotificationManager.notify(21394213, mNotification);  
    }  
}
