package com.byr.byrplayer;

import com.byr.tools.ChatRecordKeeper;
import com.byr.tools.SharedPreferenceServer;
import com.easemob.EMCallBack;
import com.easemob.chat.EMChat;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMConversation;
import com.easemob.chat.EMGroupManager;
import com.easemob.chat.EMMessage;
import com.easemob.chat.GroupChangeListener;
import com.easemob.chat.TextMessageBody;
import com.easemob.exceptions.EaseMobException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class ChatFragment extends Fragment {
	static MainpageActivity parent;
	static ToastHandler toastHandler;
	public static ListHandler listHandler;
	public static String groupId = "141647146931441";
	//private String pwd = "000000";
	private View fv;
	private ListView list;
	public static ChatFragmentListAdapter adapterChat;
	public static EditText messageContent;//用于listHandler传递参数
	public ChatFragment(MainpageActivity m){
		
		parent = m;
		toastHandler = new ToastHandler();
		listHandler = new ListHandler();
		
		EMGroupManager.getInstance().addGroupChangeListener(new GroupChangeListener() {
			@Override
			public void onUserRemoved(String groupId, String groupName) {
				//当前用户被管理员移除出群聊
			}
			@Override
			public void onInvitationReceived(String groupId, String groupName, String inviter, String reason) {
				//收到加入群聊的邀请
			}
			@Override
			public void onInvitationDeclined(String groupId, String invitee, String reason) {
				//群聊邀请被拒绝
			}
			@Override
			public void onInvitationAccpted(String groupId, String inviter, String reason) {
				//群聊邀请被接受
				show("成功加入群组");
			}
			@Override
			public void onGroupDestroy(String groupId, String groupName) {
				//群聊被创建者解散
			}
			@Override
			public void onApplicationReceived(String groupId, String groupName, String applyer, String reason) {
				//收到加群申请
			}
			@Override
			public void onApplicationAccept(String groupId, String groupName, String accepter) {
				//加群申请被同意
				show("成功加入群组");
			}
			@Override
			public void onApplicationDeclined(String groupId, String groupName, String decliner, String reason) {
				// 加群申请被拒绝
				show("加入群组失败");
			}
		});
		NewMessageBroadcastReceiver msgReceiver = new NewMessageBroadcastReceiver();
		IntentFilter intentFilter = new IntentFilter(EMChatManager.getInstance().getNewMessageBroadcastAction());
		intentFilter.setPriority(3);
		parent.registerReceiver(msgReceiver, intentFilter);
		EMChat.getInstance().setAppInited();
		
		if(SharedPreferenceServer.hasData(parent.getApplicationContext()) && SharedPreferenceServer.isAutoLogin(parent)){
			try {
				login();
			} catch (EaseMobException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			SharedPreferenceServer.removeUserData(parent);
		}
	}

	public static void login() throws EaseMobException {
		Thread thread = new Thread(new Runnable() {
		    public void run() {
		      try {
		         // 调用sdk注册方法
		    	  EMChatManager.getInstance().createAccountOnServer(SharedPreferenceServer.getUser(parent.getApplicationContext()),"000000");
		      } catch (final Exception e) {
		      }
		   }
		});
		thread.start();
		try  {
            thread.join (3000) ;
        }  catch  (InterruptedException e) {
            e.printStackTrace () ;
        }
		
		EMChatManager.getInstance().login(SharedPreferenceServer.getUser(parent.getApplicationContext()),"000000", new EMCallBack() {
		    @Override
		    public void onSuccess() {
		    // TODO Auto-generated method stub
		    	EMConversation conversation = EMChatManager.getInstance().getConversation(groupId,true);
				conversation.resetUnsetMsgCount();
				EMChatManager.getInstance().clearConversation(groupId);
				
				try {
					EMGroupManager.getInstance().joinGroup(groupId);
				} catch (EaseMobException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.d("byrtv",e.getMessage());
					show("catch 到错误："+e.getMessage());
				}
				show("登陆成功");
				
		    }
			
		    @Override
		    public void onProgress(int progress, String status) {
		    // TODO Auto-generated method stub
		    	Log.d("Huan",status + progress);
		    }
				
		    @Override
		    public void onError(int code, String message) {
		    // TODO Auto-generated method stub
		    	show("出错");
		    }
		});
		 EMChatManager.getInstance().getChatOptions().setNotifyBySoundAndVibrate(false);
	}
	
	public static void logout(){
		EMChatManager.getInstance().logout();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState){
		fv = inflater.inflate(R.layout.fragment_chat, null); 
		list = (ListView)fv.findViewById(R.id.listview_activitychat);
		messageContent = (EditText)fv.findViewById(R.id.edit_chat);
		list.setAdapter(new ChatFragmentListAdapter(parent.getApplicationContext(),ChatRecordKeeper.getList()));
		return fv;
	}
	
	public static void show(String message){
		Message msg = new Message();
        Bundle b = new Bundle();// 存放数据
        b.putString("String", message);
        msg.setData(b);
        toastHandler.sendMessage(msg);
	}
	
	void showToask(String hint) {
		Toast toast = Toast.makeText(parent, hint, Toast.LENGTH_SHORT);
		toast.show();
	}
	
	class ToastHandler extends Handler {
        public ToastHandler() {
        }

        public ToastHandler(Looper L) {
            super(L);
        }

        // 子类必须重写此方法,接受数据
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            Log.d("MyHandler", "handleMessage......");
            super.handleMessage(msg);
            // 此处可以更新UI
            Bundle b = msg.getData();
            String string = b.getString("String");
            showToask(string);
        }
    }
	
	class ListHandler extends Handler {
        public ListHandler() {
        }

        public ListHandler(Looper L) {
            super(L);
        }

        // 子类必须重写此方法,接受数据
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            Log.d("MyHandler", "handleMessage......");
            super.handleMessage(msg);
            // 此处可以更新UI
            //Bundle b = msg.getData();
            list.setAdapter(adapterChat);
            list.setSelection(ChatRecordKeeper.getList().size()-1);
        }
    }
	
	private class NewMessageBroadcastReceiver extends BroadcastReceiver {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        //消息id
	        String msgId = intent.getStringExtra("msgid");
	        //消息类型，文本，图片，语音消息等,这里返回的值为msg.type.ordinal()。
	        EMMessage message = EMChatManager.getInstance().getMessage(msgId);
	        if(Math.abs(message.getMsgTime() - System.currentTimeMillis()) < 5*60000 && !SharedPreferenceServer.isNotReceiveGroupmessage(parent)){
		        //所以消息type实际为是enum类型
		        int msgType = intent.getIntExtra("type", 0);
		        //发消息的人的username(userid)
		        String msgFrom = intent.getStringExtra("from");
		        Log.d("main", "new message id:" + msgId + " from:" + msgFrom + " type:" + msgType);
		        //更方便的方法是通过msgId直接获取整个message
		        
		        String from = message.getFrom();
		        TextMessageBody textbody = (TextMessageBody)message.getBody();
		        String content = textbody.getMessage();
		        
		        ChatRecordKeeper.addRecord(from,content,false);
		    	ChatFragment.adapterChat = new ChatFragmentListAdapter(parent.getApplicationContext(),ChatRecordKeeper.getList());
		    	ChatFragment.listHandler.sendEmptyMessage(0);
	    	}
	     }

	}
}
