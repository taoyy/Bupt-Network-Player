package com.byr.byrplayer;

import com.byr.tools.ChatRecord;
import com.byr.tools.SafeArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ChatFragmentListAdapter extends BaseAdapter {
	
	SafeArrayList<ChatRecord> list;
	LayoutInflater inflater;
	public ChatFragmentListAdapter(Context context,SafeArrayList<ChatRecord> list) {
		super();
		this.list = list;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null || ((ViewHolder) convertView.getTag()).send_by_self != list.get(position).isSentBySelf()) {
			viewHolder = new ViewHolder();
			if (list.get(position).isSentBySelf()) {
				convertView = inflater.inflate(
						R.layout.layout_chatitem_right, null);
				viewHolder.personicon = (ImageView) convertView
						.findViewById(R.id.imageview_activitychat_right);
				viewHolder.text_content = (TextView) convertView
						.findViewById(R.id.textview_activitychat_content_right);
				viewHolder.text_username = (TextView) convertView
						.findViewById(R.id.textview_activitychat_name_right);
				viewHolder.send_by_self = true;
				convertView.setTag(viewHolder);
			} else {
				convertView = inflater.inflate(
						R.layout.layout_chatitem_left, null);
				viewHolder.personicon = (ImageView) convertView
						.findViewById(R.id.imageview_activitychat_left);
				viewHolder.text_content = (TextView) convertView
						.findViewById(R.id.textview_activitychat_content_left);
				viewHolder.text_username = (TextView) convertView
						.findViewById(R.id.textview_activitychat_name_left);
				viewHolder.send_by_self = false;
				convertView.setTag(viewHolder);
			}
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.text_content.setText(list.get(position).getContent());
		viewHolder.text_username.setText(list.get(position).getUsername());
		return convertView;
	}
	static class ViewHolder {
		ImageView personicon;
		TextView text_content;
		TextView text_username;
		boolean send_by_self;
	}
	
}
