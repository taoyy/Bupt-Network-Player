package com.byr.byrplayer;

import com.byr.tools.SharedPreferenceServer;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;

public class SettingActivity extends Activity {
Activity mcontext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mcontext = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		Switch switchlogin = (Switch)this.findViewById(R.id.switchlogin);
		Switch switchgroup = (Switch)this.findViewById(R.id.switchgroup);
		switchlogin.setChecked(SharedPreferenceServer.isAutoLogin(this.getApplicationContext()));
		switchgroup.setChecked(SharedPreferenceServer.isNotReceiveGroupmessage(this.getApplicationContext()));
		switchlogin.setOnCheckedChangeListener(new OnCheckedChangeListener() {  
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub
				SharedPreferenceServer.setAutoLogin(isChecked, mcontext);
			}  
        });  
		switchgroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {  
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub
				SharedPreferenceServer.setNotReceiveGroupmessage(isChecked, mcontext);
			}  
        });  
		
	}
}