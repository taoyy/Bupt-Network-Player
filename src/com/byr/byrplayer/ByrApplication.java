package com.byr.byrplayer;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import com.darrenmowat.imageloader.library.BitmapCache;
import com.darrenmowat.imageloader.library.ImageLoader;
import com.easemob.chat.EMChat;
//import com.easemob.chat.EMChat;

public class ByrApplication extends Application {
	public static Context appContext;
	@Override
	public void onCreate() {
		super.onCreate();
		appContext = this;
		/*StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectAll().penaltyLog().build());

		StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll()
				.penaltyLog().penaltyDeath().build());*/

		ImageLoader.getInstance().setup(getApplicationContext(),
				R.drawable.ic_launcher, R.drawable.feng);
		
		EMChat.getInstance().setDebugMode(true);
		EMChat.getInstance().init(appContext);
		
	}

	@Override
	public void onLowMemory() {
		BitmapCache.getBitmapCache(this).trimMemory();
		super.onLowMemory();
	}

	public static ByrApplication getApplication(Context context) {
		return (ByrApplication) context.getApplicationContext();
	}
}
