package com.byr.byrplayer;

import io.vov.vitamio.utils.Log;

import com.byr.tools.TvListAdapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class TvListActivity extends Activity{
	Activity mcontent = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mcontent = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);
		Intent intent=getIntent();  
		String cat = intent.getStringExtra("cat");  
		ListView listView = (ListView)findViewById(R.id.list);
		TvListAdapter adapter_tvcat = new TvListAdapter(MainpageActivity.databaseHelper.getTvSets(cat),this);
		listView.setAdapter(adapter_tvcat);
		listView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int arg2,
					long arg3) {
				Intent intent = new Intent(mcontent,VideoActivity.class);
        		Bundle bundle=new Bundle();  
        		TextView urlText = (TextView)v.findViewById(R.id.url);
				String url =urlText.getText().toString();
        		bundle.putString("url",url);  
        		intent.putExtras(bundle);  
        		startActivity(intent);
			}

		});
		ImageView close = (ImageView)this.findViewById(R.id.imageView1);
		close.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				mcontent.finish();
				Log.d("netlink","clicked");
			}
			
		});
	}

}
