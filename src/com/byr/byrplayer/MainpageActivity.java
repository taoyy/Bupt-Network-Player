package com.byr.byrplayer;

import java.util.ArrayList;

import com.byr.bbsqauth.AccessTokenKeeper;
import com.byr.bbsqauth.BbsQuathInfo;
import com.byr.bbsqauth.Constants;

import cn.byrbbs.sdk.api.UserApi;
import cn.byrbbs.sdk.api.model.ResponseError;
import cn.byrbbs.sdk.api.model.User;
import cn.byrbbs.sdk.auth.BBSAuth;
import cn.byrbbs.sdk.auth.BBSAuthListener;
import cn.byrbbs.sdk.auth.Oauth2AccessToken;
import cn.byrbbs.sdk.exception.BBSException;
import cn.byrbbs.sdk.net.RequestListener;
import cn.byrbbs.sdk.utils.LogUtil;

import com.byr.database.ByrPlayerDatabaseHelper;
import com.byr.network.ConnectionServer;
import com.byr.services.IService;
import com.byr.services.MusicPlayingService;
import com.byr.tools.ChatRecordKeeper;
import com.byr.tools.SharedPreferenceServer;
import com.darrenmowat.imageloader.library.ImageLoader;
import com.easemob.EMCallBack;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMConversation;
import com.easemob.chat.EMMessage;
import com.easemob.chat.TextMessageBody;
import com.easemob.chat.EMMessage.ChatType;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

public class MainpageActivity extends FragmentActivity{
	
	android.support.v4.app.Fragment mContent;
	public static MainpageActivity mcontext;
	public static FirstFragment firstfragment;
	public static RadioFragment radiofragment;
	public static TvFragment tvfragment;
	public static ChatFragment chatfragment;
	private static final String TAG = "MainActivity";
    private ViewPager mPager;
    private ArrayList<Fragment> fragmentsList;
    private ImageView ivBottomLine;
    private ImageView tvTabActivity, tvTabGroups, tvTabFriends, tvTabChat;
    public static String addUrl[] = null;
    public static String adContent[] = null;
    public static Boolean adIsVideo[] = null;
    private int currIndex = 0;
    private int bottomLineWidth;
    private int offset = 0;
    private int position_zero;
    private int position_one;
    private int position_two;
    private int position_three;
    private Resources resources;
	public static ByrPlayerDatabaseHelper databaseHelper = null; 
	public static Handler handler;
	public static boolean radiorefreshed = true;
	public static boolean tvadrefreshed = true;
	public boolean isScrolling;
	public View ignored;
	public View adview;
	public boolean ignoreviewadded = false;
	public boolean adinignore = false;
	ResideMenu resideMenu;
	public int page = 0;
	private BBSAuth mBBSAuth;
	private Oauth2AccessToken mAccessToken;
	private UserApi mUserApi;
	public enum modeSelect {
        Radiosel,Tvsel;    
    }
	
	/**
	 * 下面这段是音乐播放的service需要的依赖
	 */
	static IService iService;
	Intent serviceIntent;
	MyConn conn;
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		if (!io.vov.vitamio.LibsChecker.checkVitamioLibs(this))
			return;
		setContentView(R.layout.activity_mainpage);
		
		/**
    	 * 下面这段是音乐播放的service需要的依赖
    	 */
        serviceIntent = new Intent(this, MusicPlayingService.class);
		conn = new MyConn();
		bindService(serviceIntent, conn, Context.BIND_AUTO_CREATE);
		Log.d("byrplayerService","service connection begin");
		/**
		 * 下面是程序肢体部分依赖
		 */
		mBBSAuth = new BBSAuth(this, Constants.APP_KEY, Constants.REDIRECT_URL, Constants.SCOPE);
		mAccessToken = AccessTokenKeeper.readAccessToken(this);
		mUserApi = new UserApi(mAccessToken);
		BbsQuathInfo.mBBSAuth = this.mBBSAuth;
		BbsQuathInfo.mAccessToken = this.mAccessToken;
		BbsQuathInfo.mUserApi = this.mUserApi;
		
		if (mAccessToken.isSessionValid()) {
            //如果授权存
        }
		mcontext = this;
		databaseHelper = new ByrPlayerDatabaseHelper(this);
		databaseHelper.getReadableDatabase();
    	
		firstfragment= new FirstFragment(this);
		radiofragment= new RadioFragment(this);
		tvfragment = new TvFragment(this);
		//chatfragment = new ChatFragment(this);
		mContent = firstfragment;
		

		resources = getResources();
        InitWidth();
        InitTextView();
        InitViewPager();
        handler = new Handler() {  
            @Override  
            public void handleMessage(Message msg) {  
                if (msg.what == 1) {  
                	MainpageActivity.radiofragment.radioAdapter.changeCursor(MainpageActivity.databaseHelper.getAllRadioSets());  
                	radiorefreshed = true;
                }else if(msg.what == 2){
                	if(FirstFragment.recommendAdapter != null){
                		FirstFragment.recommendAdapter.changeCursor(MainpageActivity.databaseHelper.getAllRecommend());
                	}
                	tvadrefreshed = true;
                }else if (msg.what == -1) {  
                	showToask("电台更新成功，Byrer们祝你有个好心情");
                }else if (msg.what == -2) {  
                	showToask("一会儿再试吧，没有收到数据呢。。。");
                }else if(msg.what == -3){
                	showToask("页面更新成功，Byrer们祝你有个好心情");
                }else if(msg.what == -4){
                	showToask("页面获取失败，不好意思啦~~");
                }else if(msg.what == -10){
                	adUrlRefresh();
                }else if(msg.what == 10){
                	if(!adinignore && page == 0){
                		resideMenu.addIgnoredView(firstfragment.adview);
                		adinignore = true;
                	}
                }else if(msg.what == 11){
                	if(adinignore){
                		resideMenu.removeIgnoredView(firstfragment.adview);
                		adinignore = false;
                	}
                }
            }  
        };  
     // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.menu_background);
        resideMenu.attachToActivity(this);

        // create menu items;
        String titles[] = {  "清除缓存","关于", "退出" };
        int icon[] = { R.drawable.icon_settings,R.drawable.icon_calendar, R.drawable.icon_profile  };

        for (int i = 0; i < titles.length; i++){
            ResideMenuItem item = new ResideMenuItem(this, icon[i], titles[i]);
            if(i == -1){
            	item.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(mcontext,UserLoginActivity.class);
		        		startActivity(intent);
						
					}
	            });
            }else if(i == 0){
            	item.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						//mBBSAuth.authorize(new AuthListener());
						//Intent intent = new Intent(mcontext,SettingActivity.class);
		        		//startActivity(intent);
						ImageLoader.getInstance().clearCache(mcontext);
						mcontext.showToask("缓存清理完成");
					}
	            });
            }else if(i == 1){
	            item.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(mcontext,AboutActivity.class);
		        		startActivity(intent);
					}
	            });
            }else if(i == 2){
	            item.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(SharedPreferenceServer.hasData(mcontext)){
							try{
								//EMChatManager.getInstance().logout();
							}catch(Exception e){
								
							}
						}
						quit();
					}
	            });
            }
            resideMenu.addMenuItem(item,  ResideMenu.DIRECTION_LEFT); // or  ResideMenu.DIRECTION_RIGHT
        }
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        //resideMenu.addIgnoredView(this.findViewById(R.id.vPager));
        ignored = this.findViewById(R.id.vPager);
        new Thread(){
        	public void run(){
        			ConnectionServer.sendHello();
        			ConnectionServer.requireRadioUpdate();
        			ConnectionServer.requireTvUpdate();
        			ConnectionServer.requireAd();
        			ConnectionServer.requireRecommend();
        	}
        }.start();
        
	}
	
	@Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }
	
	private void InitTextView() {
        tvTabActivity = (ImageView) findViewById(R.id.tv_tab_activity);
        tvTabGroups = (ImageView) findViewById(R.id.tv_tab_groups);
        tvTabFriends = (ImageView) findViewById(R.id.tv_tab_friends);
        //tvTabChat = (ImageView) findViewById(R.id.tv_tab_chat);

        tvTabActivity.setOnClickListener(new MyOnClickListener(0));
        tvTabGroups.setOnClickListener(new MyOnClickListener(1));
        tvTabFriends.setOnClickListener(new MyOnClickListener(2));
        //tvTabChat.setOnClickListener(new MyOnClickListener(3));
    }
	
	private void InitViewPager() {
        mPager = (ViewPager) findViewById(R.id.vPager);
        fragmentsList = new ArrayList<Fragment>();
            
        fragmentsList.add(firstfragment);
        fragmentsList.add(radiofragment);
        fragmentsList.add(tvfragment);
        //fragmentsList.add(chatfragment);
        
        mPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager(), fragmentsList));
        mPager.setCurrentItem(0);
        mPager.setOnPageChangeListener(new MyOnPageChangeListener());
        mPager.setOffscreenPageLimit(4);  
    }

    private void InitWidth() {
        ivBottomLine = (ImageView) findViewById(R.id.iv_bottom_line);
        bottomLineWidth = ivBottomLine.getLayoutParams().width;
        Log.d(TAG, "cursor imageview width=" + bottomLineWidth);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenW = dm.widthPixels;
        offset = (int) ((screenW / 4.0 - bottomLineWidth) / 2);
        Log.i("MainActivity", "offset=" + offset);
        //int off = bottomLineWidth / 2;
        position_zero = (int) (screenW / 6) * 1 - 35;
        position_one = (int) (screenW / 6) * 3 - 35;
        position_two = (int) (screenW / 6) * 5 - 35;
        position_three = (int) (screenW / 3.0) * 3 + position_zero;
        Animation animation = new TranslateAnimation(position_one, position_zero, 0, 0);
        animation.setFillAfter(true);
        animation.setDuration(0);
        ivBottomLine.startAnimation(animation);
    }
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	public class MyOnClickListener implements View.OnClickListener {
        private int index = 0;

        public MyOnClickListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
            mPager.setCurrentItem(index);
        }
    };

    public class MyOnPageChangeListener implements OnPageChangeListener {

        @Override
        public void onPageSelected(int arg0) {
        	changePage(arg0);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        	 
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        	
        }
    }
    
    public void changePage(int arg0){
    	 Animation animation = null;
         switch (arg0) {
         case 0:
             if (currIndex == 1) {
                 animation = new TranslateAnimation(position_one, position_zero, 0, 0);
                 //tvTabGroups.setTextColor(resources.getColor(R.color.lightwhite));
             } else if (currIndex == 2) {
                 animation = new TranslateAnimation(position_two, position_zero, 0, 0);
                 //tvTabFriends.setTextColor(resources.getColor(R.color.lightwhite));
             } else if (currIndex == 3) {
                 animation = new TranslateAnimation(position_three, position_zero, 0, 0);
                 //tvTabChat.setTextColor(resources.getColor(R.color.lightwhite));
             }
             //tvTabActivity.setTextColor(resources.getColor(R.color.white));
             break;
         case 1:
             if (currIndex == 0) {
                 animation = new TranslateAnimation(position_zero, position_one, 0, 0);
                // tvTabActivity.setTextColor(resources.getColor(R.color.lightwhite));
             } else if (currIndex == 2) {
                 animation = new TranslateAnimation(position_two, position_one, 0, 0);
                 //tvTabFriends.setTextColor(resources.getColor(R.color.lightwhite));
             } else if (currIndex == 3) {
                 animation = new TranslateAnimation(position_three, position_one, 0, 0);
                 //tvTabChat.setTextColor(resources.getColor(R.color.lightwhite));
             }
             //tvTabGroups.setTextColor(resources.getColor(R.color.white));
             break;
         case 2:
             if (currIndex == 0) {
                 animation = new TranslateAnimation(position_zero, position_two, 0, 0);
                 //tvTabActivity.setTextColor(resources.getColor(R.color.lightwhite));
             } else if (currIndex == 1) {
                 animation = new TranslateAnimation(position_one, position_two, 0, 0);
                 //tvTabGroups.setTextColor(resources.getColor(R.color.lightwhite));
             } else if (currIndex == 3) {
                 animation = new TranslateAnimation(position_three, position_two, 0, 0);
                 //tvTabChat.setTextColor(resources.getColor(R.color.lightwhite));
             }
             //tvTabFriends.setTextColor(resources.getColor(R.color.white));
             break;
         case 3:
             if (currIndex == 0) {
                 animation = new TranslateAnimation(position_zero, position_three, 0, 0);
                 //tvTabActivity.setTextColor(resources.getColor(R.color.lightwhite));
             } else if (currIndex == 1) {
                 animation = new TranslateAnimation(position_one, position_three, 0, 0);
                 //tvTabGroups.setTextColor(resources.getColor(R.color.lightwhite));
             } else if (currIndex == 2) {
                 animation = new TranslateAnimation(position_two, position_three, 0, 0);
                 //tvTabFriends.setTextColor(resources.getColor(R.color.lightwhite));
             }
             //tvTabChat.setTextColor(resources.getColor(R.color.white));
             break;
         }
         //showToask(String.valueOf(arg0));
         page = arg0;
     	if(arg0 == 0){
     		if(ignoreviewadded){
     			resideMenu.removeIgnoredView(ignored);
     			//resideMenu.addIgnoredView(tvfragment.adview);
     			//resideMenu.addIgnoredView(ignored);
     			ignoreviewadded = false;
     		}
     		Message msg = new Message();  
	            msg.what = 10;  
	            MainpageActivity.handler.sendMessage(msg);  
         }else{
         	if(!ignoreviewadded){
         		resideMenu.addIgnoredView(ignored);
         		//resideMenu.removeIgnoredView(tvfragment.adview);
         		ignoreviewadded = true;
         	}
         	Message msg = new Message();  
	            msg.what = 11;  
	            MainpageActivity.handler.sendMessage(msg);  
         }
         currIndex = arg0;
         animation.setFillAfter(true);
         animation.setDuration(300);
         ivBottomLine.startAnimation(animation);
    }
    void showToask(String hint) {
		Toast toast = Toast.makeText(this, hint, Toast.LENGTH_SHORT);
		toast.show();
	}
    
    void adUrlRefresh(){
    	firstfragment.changePageAdapter();
    }
    void quit(){
    	this.finish();
    }
    public void byrlive(View v){
    	//changePage(1);
    	mPager.setCurrentItem(1);
    }

    public void cctv(View v){
    	Intent intent = new Intent(this,TvListActivity.class);
		Bundle bundle=new Bundle();  
		bundle.putString("cat","央视");  
		intent.putExtras(bundle);  
		startActivity(intent);
    }
    
    public void local(View v){
    	Intent intent = new Intent(this,TvListActivity.class);
		Bundle bundle=new Bundle();  
		bundle.putString("cat","地方卫视");  
		intent.putExtras(bundle);  
		startActivity(intent);
    }
    
    public void hd(View v){
    	Intent intent = new Intent(this,TvListActivity.class);
		Bundle bundle=new Bundle();  
		bundle.putString("cat","高清");  
		intent.putExtras(bundle);  
		startActivity(intent);
    }
    
    public void send(View v){
    	if(SharedPreferenceServer.hasData(mcontext.getApplicationContext())){
	    	EMConversation conversation = EMChatManager.getInstance().getConversation(ChatFragment.groupId,true);
			//创建一条文本消息
			EMMessage message = EMMessage.createSendMessage(EMMessage.Type.TXT);
			//如果是群聊，设置chattype,默认是单聊
			message.setChatType(ChatType.GroupChat);
			message.setFrom(SharedPreferenceServer.getUser(mcontext.getApplicationContext()));
			//设置消息body
			TextMessageBody txtBody = new TextMessageBody(ChatFragment.messageContent.getText().toString());
			message.addBody(txtBody);
			//设置接收人
			message.setReceipt(ChatFragment.groupId);
			//把消息加入到此会话对象中
			conversation.addMessage(message);
			//发送消息
			EMChatManager.getInstance().sendMessage(message, new MessageSendListener());
			
    	}else{
    		showToask("没登陆，吐槽不了了吧？");
    		Intent intent = new Intent(mcontext,UserLoginActivity.class);
    		startActivity(intent);
    	}
    }
    
    /**
     * stored token informations(token OR error code)  after handle redirect url
     * 
     */
    class AuthListener implements BBSAuthListener {
        
        @Override
        public void onComplete(Bundle values) {
            // 从 Bundle 中解析 Token
            mAccessToken = Oauth2AccessToken.parseAccessToken(values);
            if (mAccessToken.isSessionValid()) {
            	// show it
                // store Token into SharedPreferences
            	try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	mUserApi = new UserApi(mAccessToken);
            	mUserApi.show(mListener);
            	showToask("登陆成功");
            } else {
                // 以下几种情况，您会收到 Code：
                // 1. 当您未在平台上注册的应用程序的包名与签名时；
                // 2. 当您注册的应用程序包名与签名不正确时；
                // 3. 当您在平台上注册的包名和签名与您当前测试的应用的包名和签名不匹配时。
                //String code = values.getString("code");
                showToask("签名问题");
            }
        }

        @Override
        public void onCancel() {
        	showToask("取消登陆");
        }

        @Override
        public void onException(BBSException e) {
        	e.printStackTrace();
        }
    }
    
	private RequestListener mListener = new RequestListener() {
        @Override
        public void onComplete(String response) {
        	showToask("enter");
            if (!TextUtils.isEmpty(response)) {
                LogUtil.i(TAG, response);
                
                ResponseError error = ResponseError.parse(response);
                if(error != null){
                    showToask("并不存在登陆账号");
                }
                
                User user = User.parse(response);
                if (user != null) {
                	showToask("登陆成功 user id: " + user.id);
                } else {
                	showToask(response);
                }
            }
        }

		@Override
		public void onException(BBSException e) {
			// TODO Auto-generated method stub
            LogUtil.e(TAG, e.getMessage());
		}
    };
    
    private class MessageSendListener implements EMCallBack{

		@Override
		public void onError(int arg0, String arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProgress(int arg0, String arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onSuccess() {
			// TODO Auto-generated method stub
			//显示信息
			ChatRecordKeeper.addRecord(SharedPreferenceServer.getUser(mcontext.getApplicationContext()),ChatFragment.messageContent.getText().toString(),true);
	    	ChatFragment.adapterChat = new ChatFragmentListAdapter(mcontext.getApplicationContext(),ChatRecordKeeper.getList());
	    	ChatFragment.listHandler.sendEmptyMessage(0);
	    	ChatFragment.messageContent.setText("");
		}
    	
    }
    
    /**
	 * 下面这段是音乐播放的service需要的依赖
	 */
    private class MyConn implements ServiceConnection {

		// 绑定一个服务成功的时候 调用 onServiceConnected
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			iService = (IService) service;
			Log.d("byrplayerService","service connection successful");
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {

		}
	}
    
    @Override
	protected void onDestroy() {
    	if(conn != null)
    		unbindService(conn);
    	if(RadioFragment.mNotificationManager != null)
    		RadioFragment.mNotificationManager.cancel(21394213);
		super.onDestroy();
	}
    
    
}
