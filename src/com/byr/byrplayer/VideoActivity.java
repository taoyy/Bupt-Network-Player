package com.byr.byrplayer;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class VideoActivity extends Activity{
	private String path = "http://live.3gv.ifeng.com/zixun.m3u8";
	private VideoView mVideoView;
	Handler handler;
	private ProgressDialog m_pDialog;
	Activity mcontext;
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		mcontext = this;
		if (!io.vov.vitamio.LibsChecker.checkVitamioLibs(this))
			return;
		setContentView(R.layout.activity_video);
		handler = new Handler() {  
            @Override  
            public void handleMessage(Message msg) {  
                if (msg.what == 1) {  
                	m_pDialog = new ProgressDialog(mcontext);

        			// 设置进度条风格，风格为圆形，旋转的
        			m_pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        			        
        			        // 设置ProgressDialog 提示信息
        			m_pDialog.setMessage("正在连接资源。。。");
        			        
        			        // 设置ProgressDialog 的进度条是否不明确
        			m_pDialog.setIndeterminate(false);
        			           
        			        // 设置ProgressDialog 是否可以按退回按键取消
        			m_pDialog.setCancelable(true);
        			       
        			m_pDialog.show();
                }else if (msg.what == 2) { 
                	if(m_pDialog != null)
                		m_pDialog.hide();
                }
            }
        };  
		Intent intent=getIntent();  
		path = intent.getStringExtra("url");  
		mVideoView = (VideoView) findViewById(R.id.surface_view);
		mVideoView.setVideoPath(path);
		mVideoView.setVideoQuality(MediaPlayer.VIDEOQUALITY_LOW);
		
		
		mVideoView.setBufferSize(2048);
        mVideoView.setMediaController(new MediaController(this));
        mVideoView.requestFocus();
        // this.myVideoView.setOnBufferingUpdateListener(this.mBufferingUpdateListener);

        mVideoView
                .setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        // optional need Vitamio 4.0
                    	Message msg = new Message();
                 		msg.what = 2;
                 		handler.sendMessage(msg);
                        mediaPlayer.setPlaybackSpeed(1.0f);
                    }
                });
        Message msg = new Message();
		msg.what = 1;
		handler.sendMessage(msg);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		if (mVideoView != null)
			mVideoView.setVideoLayout(VideoView.VIDEO_LAYOUT_SCALE, 0);
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		mVideoView.stopPlayback();
		Log.d("byrmediaplay","resource released");
	}
}
