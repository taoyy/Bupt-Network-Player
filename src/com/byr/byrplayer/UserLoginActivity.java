package com.byr.byrplayer;
import com.byr.bbsqauth.AccessTokenKeeper;
import com.byr.bbsqauth.BbsQuathInfo;
import com.byr.bbsqauth.Constants;
import com.byr.tools.SharedPreferenceServer;
import com.easemob.exceptions.EaseMobException;

import cn.byrbbs.sdk.api.UserApi;
import cn.byrbbs.sdk.api.model.ResponseError;
import cn.byrbbs.sdk.api.model.User;
import cn.byrbbs.sdk.auth.BBSAuth;
import cn.byrbbs.sdk.auth.BBSAuthListener;
import cn.byrbbs.sdk.auth.Oauth2AccessToken;
import cn.byrbbs.sdk.exception.BBSException;
import cn.byrbbs.sdk.net.RequestListener;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class UserLoginActivity extends Activity {
	Activity mcontext;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mcontext = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_userlogin);
		String bbsurl = Constants.REDIRECT_URL;
		if(SharedPreferenceServer.hasBBSUrl(mcontext)){
			bbsurl = SharedPreferenceServer.getBBSUrl(mcontext);
		}
		BbsQuathInfo.mBBSAuth = new BBSAuth(this, Constants.APP_KEY, bbsurl, Constants.SCOPE);
		BbsQuathInfo.mAccessToken = AccessTokenKeeper.readAccessToken(this);
		BbsQuathInfo.mUserApi = new UserApi(BbsQuathInfo.mAccessToken);
		Log.d("byrplayerlogin","from UserLoginAct Line33 , stored data is:" + SharedPreferenceServer.getUser(this.getApplicationContext()));
		if(SharedPreferenceServer.hasData(this.getApplicationContext())){
			Intent intent = new Intent(this,UserIdActivity.class);
			startActivity(intent);
			this.finish();
		}
	}
	
	public void login(View view){
		BbsQuathInfo.mBBSAuth.authorize(new AuthListener());
	}
	
	/**
     * stored token informations(token OR error code)  after handle redirect url
     * 
     */
    class AuthListener implements BBSAuthListener {
        
        @Override
        public void onComplete(Bundle values) {
            // 从 Bundle 中解析 Token
        	BbsQuathInfo.mAccessToken = Oauth2AccessToken.parseAccessToken(values);
            if (BbsQuathInfo.mAccessToken.isSessionValid()) {
            	// show it
                // store Token into SharedPreferences
            	try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	BbsQuathInfo.mUserApi = new UserApi(BbsQuathInfo.mAccessToken);
            	BbsQuathInfo.mUserApi.show(mListener);
            } else {
                // 以下几种情况，您会收到 Code：
                // 1. 当您未在平台上注册的应用程序的包名与签名时；
                // 2. 当您注册的应用程序包名与签名不正确时；
                // 3. 当您在平台上注册的包名和签名与您当前测试的应用的包名和签名不匹配时。
                String code = values.getString("code");
                showToask("出了一个问题，编号"+code);
            }
        }

        @Override
        public void onCancel() {
        	showToask("取消登陆");
        }

        @Override
        public void onException(BBSException e) {
        	e.printStackTrace();
        }
    }
	
	private RequestListener mListener = new RequestListener() {
        @Override
        public void onComplete(String response) {
            if (!TextUtils.isEmpty(response)) {
                
                ResponseError error = ResponseError.parse(response);
                if(error != null){
                    showToask("并不存在登陆账号");
                }
                
                User user = User.parse(response);
                if (user != null) {
                	SharedPreferenceServer.saveUserData(user.id, mcontext.getApplicationContext());
                	Log.d("byrplayerlogin","from UserLoginAct Line102 , stored data is:" + SharedPreferenceServer.getUser(mcontext.getApplicationContext()));
                	if(SharedPreferenceServer.hasData(mcontext.getApplicationContext())){
            			try {
							ChatFragment.login();
						} catch (EaseMobException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
                		Intent intent = new Intent(mcontext,UserIdActivity.class);
            			startActivity(intent);
            			mcontext.finish();
            		}
                } else {
                	showToask(response);
                }
            }
        }
        
		@Override
		public void onException(BBSException arg0) {
			// TODO Auto-generated method stub
			
		}
     };
     void showToask(String hint) {
 		Toast toast = Toast.makeText(mcontext, hint, Toast.LENGTH_SHORT);
 		toast.show();
 	}
}
