package com.byr.byrplayer;

import com.byr.tools.SharedPreferenceServer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class UserIdActivity extends Activity {
	Activity mcontext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mcontext = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_userid);
		TextView textUsername = (TextView)this.findViewById(R.id.testUser);
		String username = "��ã�"+SharedPreferenceServer.getUpperUser(this.getApplicationContext());
		textUsername.setText(username.toCharArray(),0,username.length());
	}
	
	public void logout(View view){
		SharedPreferenceServer.removeUserData(this.getApplicationContext());
		if(!SharedPreferenceServer.hasData(this.getApplicationContext())){
			ChatFragment.logout();
			Intent intent = new Intent(this,UserLoginActivity.class);
			startActivity(intent);
			this.finish();
		}
	}
}
