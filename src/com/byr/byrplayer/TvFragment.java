package com.byr.byrplayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class TvFragment extends Fragment{
	static MainpageActivity parent;
	View fv;
	
	public TvFragment(MainpageActivity m){
		parent = m;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		fv = inflater.inflate(R.layout.fragment_tv, null); 
		
		ImageView linCctv = (ImageView)fv.findViewById(R.id.cat1);
		linCctv.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(parent,TvListActivity.class);
				Bundle bundle=new Bundle();  
        		bundle.putString("cat","央视");  
        		intent.putExtras(bundle);  
        		startActivity(intent);
			}
			
		});
		
		ImageView linLocal = (ImageView)fv.findViewById(R.id.cat2);
		linLocal.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(parent,TvListActivity.class);
				Bundle bundle=new Bundle();  
        		bundle.putString("cat","地方卫视");  
        		intent.putExtras(bundle);  
        		startActivity(intent);
			}
			
		});
		
		ImageView linHd = (ImageView)fv.findViewById(R.id.cat3);
		linHd.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(parent,TvListActivity.class);
				Bundle bundle=new Bundle();  
        		bundle.putString("cat","高清");  
        		intent.putExtras(bundle);  
        		startActivity(intent);
			}
			
		});
		
		ImageView linOther = (ImageView)fv.findViewById(R.id.cat4);
		linOther.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(parent,TvListActivity.class);
				Bundle bundle=new Bundle();  
        		bundle.putString("cat","其他");  
        		intent.putExtras(bundle);  
        		startActivity(intent);
			}
			
		});
		/*
		ExpandableListView expandableListView =(ExpandableListView)fv.findViewById(R.id.list);
        TvAdapter adapter_tvcat = new TvAdapter(MainpageActivity.databaseHelper.getAllTvCats(),parent);
        //在这里设置了适配器
        expandableListView.setAdapter(adapter_tvcat);
        expandableListView.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,int groupPosition, int childPosition, long id) {
            		
            		Intent intent = new Intent(TvFragment.parent,VideoActivity.class);
            		Bundle bundle=new Bundle();  
            		if(groupPosition == 0 && childPosition == 4){
            			bundle.putString("url","rtsp://10.3.220.249/CCTV-5");  
            		}else if(groupPosition == 2 && childPosition == 3){
            			bundle.putString("url","http://live.3gv.ifeng.com/zhongwen.m3u8");  
            		}else if(groupPosition == 2 && childPosition == 4){
            			bundle.putString("url","rtmp://10.3.18.186:1935/vod/BillGates.mp4");  
            		}else{
            			bundle.putString("url","http://live.3gv.ifeng.com/zixun.m3u8");  
            		}
            		intent.putExtras(bundle);  
            		startActivity(intent);
                	return false;
            }
        });
        */
		return fv;
	}
}
