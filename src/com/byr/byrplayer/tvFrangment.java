package com.byr.byrplayer;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class tvFrangment extends Fragment{
	static MainpageActivity parent;
	public tvFrangment(MainpageActivity m){
		parent = m;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View fv = inflater.inflate(R.layout.fragment_radio, null); 
		final ExpandableListAdapter adapter = new BaseExpandableListAdapter() {
            //设置组视图的图片
            int[] logos = new int[] { R.drawable.cctv5, R.drawable.nasa,R.drawable.logo};
            //设置组视图的显示文字
            private String[] generalsTypes = new String[] { "CCTV", "地方卫视", "其他节目" };
            //子视图显示文字
            private String[][] generals = new String[][] {
                    { "CCTV-1", "CCTV-2", "CCTV-3", "CCTV-4", "CCTV-5", "CCTV-6" },
                    { "湖南卫视", "江苏卫视", "北京卫视", "辽宁卫视", "东南卫视", "辽宁卫视" },
                    { "NASA", "BBC", "ABC", "凤凰卫视", "科普" }

            };
            //子视图图片
            public int[][] generallogos = new int[][] {
                    { R.drawable.ic_launcher, R.drawable.ic_launcher,
                            R.drawable.ic_launcher, R.drawable.ic_launcher,
                            R.drawable.cctv5, R.drawable.ic_launcher },
                    { R.drawable.ic_launcher, R.drawable.ic_launcher,
                            R.drawable.ic_launcher, R.drawable.ic_launcher,
                            R.drawable.ic_launcher, R.drawable.ic_launcher },
                    { R.drawable.ic_launcher, R.drawable.ic_launcher,
                            R.drawable.ic_launcher, R.drawable.feng,
                            R.drawable.nasa} };
            
            //自己定义一个获得文字信息的方法
            TextView getTextView() {
                AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
                        ViewGroup.LayoutParams.FILL_PARENT, 64);
                TextView textView = new TextView(
                		parent);
                textView.setLayoutParams(lp);
                textView.setGravity(Gravity.CENTER_VERTICAL);
                textView.setPadding(36, 0, 0, 0);
                textView.setTextSize(20);
                textView.setTextColor(Color.BLACK);
                return textView;
            }

            
            //重写ExpandableListAdapter中的各个方法
            @Override
            public int getGroupCount() {
                // TODO Auto-generated method stub
                return generalsTypes.length;
            }

            @Override
            public Object getGroup(int groupPosition) {
                // TODO Auto-generated method stub
                return generalsTypes[groupPosition];
            }

            @Override
            public long getGroupId(int groupPosition) {
                // TODO Auto-generated method stub
                return groupPosition;
            }

            @Override
            public int getChildrenCount(int groupPosition) {
                // TODO Auto-generated method stub
                return generals[groupPosition].length;
            }

            @Override
            public Object getChild(int groupPosition, int childPosition) {
                // TODO Auto-generated method stub
                return generals[groupPosition][childPosition];
            }

            @Override
            public long getChildId(int groupPosition, int childPosition) {
                // TODO Auto-generated method stub
                return childPosition;
            }

            @Override
            public boolean hasStableIds() {
                // TODO Auto-generated method stub
                return true;
            }

            @Override
            public View getGroupView(int groupPosition, boolean isExpanded,
                    View convertView, ViewGroup parent) {
                // TODO Auto-generated method stub
                LinearLayout ll = new LinearLayout(
                		tvFrangment.parent);
                ll.setOrientation(0);
                ImageView logo = new ImageView(tvFrangment.parent);
                logo.setImageResource(logos[groupPosition]);
                logo.setPadding(50, 0, 0, 0);
                ll.addView(logo);
                TextView textView = getTextView();
                textView.setTextColor(Color.BLACK);
                textView.setText(getGroup(groupPosition).toString());
                ll.addView(textView);

                return ll;
            }

            @Override
            public View getChildView(int groupPosition, int childPosition,
                    boolean isLastChild, View convertView, ViewGroup parent) {
                // TODO Auto-generated method stub
                LinearLayout ll = new LinearLayout(
                		tvFrangment.parent);
                ll.setOrientation(0);
                ImageView generallogo = new ImageView(
                		tvFrangment.parent);
                generallogo
                        .setImageResource(generallogos[groupPosition][childPosition]);
                ll.addView(generallogo);
                TextView textView = getTextView();
                textView.setText(getChild(groupPosition, childPosition)
                        .toString());
                ll.addView(textView);
                return ll;
            }

            @Override
            public boolean isChildSelectable(int groupPosition,
                    int childPosition) {
                // TODO Auto-generated method stub
                return true;
            }

        };
        ExpandableListView expandableListView =(ExpandableListView)fv.findViewById(R.id.list);
        expandableListView.setAdapter(adapter);
        expandableListView.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,int groupPosition, int childPosition, long id) {
            		
            		Intent intent = new Intent(tvFrangment.parent,VideoActivity.class);
            		Bundle bundle=new Bundle();  
            		if(groupPosition == 0 && childPosition == 4){
            			bundle.putString("url","rtsp://10.3.220.249/CCTV-5");  
            		}else if(groupPosition == 2 && childPosition == 3){
            			bundle.putString("url","http://live.3gv.ifeng.com/zhongwen.m3u8");  
            		}else if(groupPosition == 2 && childPosition == 4){
            			bundle.putString("url","rtmp://10.3.18.186:1935/vod/BillGates.mp4");  
            		}else{
            			bundle.putString("url","http://live.3gv.ifeng.com/zixun.m3u8");  
            		}
            		intent.putExtras(bundle);  
            		startActivity(intent);
                	return false;
            }
        });
        
		return fv;
	}
	
	void showToask(String hint) {
		Toast toast = Toast.makeText(parent, hint, Toast.LENGTH_SHORT);
		toast.show();
	}
	
}
