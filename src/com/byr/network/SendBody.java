package com.byr.network;

import java.io.Serializable;
@SuppressWarnings("serial")
public class SendBody implements Serializable{
	public int REQUEST_TYEP = -1;
	/**
	 * 下面的几种数据代表了客户端的不同请求，是RequestType的具体值
	 */
	public static int TYPE_UNDEFINED = -1;
	public static int TYPE_HELLO = 0;
	public static int TYPE_GET_STATUS = 1;
	public static int TYPE_GET_RADIO_UPDATE = 2;
	public static int TYPE_GET_TV_UPDATE = 3;
	public static int TYPE_GET_AD = 4;
	public static int TYPE_GET_OTHER = 5;
	public String GET_OTHER_NAME = null;
	public static String NAME = "SendBody";
	
	public SendBody(int type){
		this.REQUEST_TYEP = type;
	}
	
	public int getREQUEST_TYEP() {
		return REQUEST_TYEP;
	}
	
	public void setREQUEST_TYEP(int REQUEST_TYEP) {
		this.REQUEST_TYEP = REQUEST_TYEP;
	}

}
