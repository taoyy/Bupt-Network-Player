package com.byr.network;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.List;

import com.byr.network.RadioInfo.RadioSetInfo;
import com.byr.network.TvInfo.TvSetInfo;

@SuppressWarnings("serial")
public class ReplyBody implements Serializable{
	
	public int REPLY_TYEP = -1;
	/**
	 * 下面的几种数据代表了客户端的不同请求，是ReplyType的具体值
	 */
	public static int TYPE_UNDEFINED = -1;
	public static int TYPE_HELLO_TOO = 0;
	public static int TYPE_STATUS_REPORT = 1;
	public static int TYPE_RADIO_UPDATE = 2;
	public static int TYPE_TV_UPDATE = 3;
	public static int TYPE_AD = 4;
	public RadioInfo radioinfo = null;
	public TvInfo tvinfo = null;
	public String[] adUrl = null;
	public String[] adContent = null;
	public Boolean[] adIsVideo = null;
	public String NAME = "ReplyBody";

	public ReplyBody(int type){
		REPLY_TYEP = type;
	}
	
	public boolean setRadioInfo(ResultSet rseult){
		REPLY_TYEP = TYPE_RADIO_UPDATE;
		try{
			radioinfo  = new RadioInfo(rseult);
			return true;
		}catch(Exception e){
			return false;
		}finally{
			
		}
	}
	
	public boolean setTvInfo(ResultSet rseult){
		REPLY_TYEP = TYPE_TV_UPDATE;
		try{
			tvinfo  = new TvInfo(rseult);
			return true;
		}catch(Exception e){
			return false;
		}finally{
			
		}
	}
	
	public List<RadioSetInfo> getRadioInfo(){
		if(radioinfo != null){
			return radioinfo.radioList;
		}else{
			return null;
		}
	}
	
	public List<TvSetInfo> getTvInfo(){
		if(tvinfo != null){
			return tvinfo.tvList;
		}else{
			return null;
		}
	}
}
