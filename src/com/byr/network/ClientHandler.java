package com.byr.network;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import com.byr.bbsqauth.Constants;
import com.byr.byrplayer.MainpageActivity;
import com.byr.byrplayer.R;
import com.byr.byrplayer.FirstFragment;
import com.byr.network.RadioInfo.RadioSetInfo;
import com.byr.network.RecommendInfo.RecommendSetInfo;
import com.byr.network.TvInfo.TvSetInfo;
import com.byr.tools.SharedPreferenceServer;


import android.os.Message;
import android.util.Log;

public class ClientHandler extends IoHandlerAdapter {
	 	public void messageReceived(IoSession session, Object message) throws Exception { 
	        System.out.println("收到服务器消息：" + message.toString()); 
	        Log.d("netlink","收到服务器消息：" + " 处理中。。");
	        
	        if(message instanceof ReplyBody){
	        	ReplyBody replybody = (ReplyBody)message;
	        	Log.d("netlink","Kind: " + replybody.REPLY_TYEP);
	        	if(replybody.REPLY_TYEP == ReplyBody.TYPE_HELLO_TOO){
	        		//Constants.REDIRECT_URL = replybody.NAME;
	        		if(!replybody.NAME.equals("ReplyBody")){
	        			SharedPreferenceServer.saveBBSUrl(replybody.NAME,MainpageActivity.mcontext.getApplicationContext());
	        		}
	        		Log.d("netlink","收到服务器问候消息");
	        	}else if(replybody.REPLY_TYEP == ReplyBody.TYPE_STATUS_REPORT){
	        		Log.d("netlink","收到服务器状态信息");
	        	}else if(replybody.REPLY_TYEP == ReplyBody.TYPE_RADIO_UPDATE){
	        		Log.d("netlink","收到服务器更新信息");
	        		if(replybody.getRadioInfo().size() != 0)
	        			MainpageActivity.databaseHelper.deleteAllRadioSet();
	        		for(int i = 0;i < replybody.getRadioInfo().size(); i ++){
	        			RadioSetInfo info = replybody.getRadioInfo().get(i);
	        			Log.d("netlink","+--"+info.name+"---"+info.desc+"---"+info.url+"--+");
	        			if(MainpageActivity.databaseHelper != null){
	        				MainpageActivity.databaseHelper.tryToInsertRadioSet(info.name, info.desc, info.url, R.drawable.feng);
	        			}
	                }
	        		try{
        				Message msg = new Message();  
        	            msg.what = 1;  
        	            MainpageActivity.handler.sendMessage(msg);  
        			}catch(Exception e){
        				Log.d("netlink",e.toString());
        			}
	        		Log.d("netlink","收到服务器更新收音机频道信息"+replybody.getRadioInfo().size());
	        		
	        	}else if(replybody.REPLY_TYEP == ReplyBody.TYPE_TV_UPDATE){
	        		Log.d("netlink","收到服务器电视更新信息");
	        		if(replybody.getTvInfo().size() != 0)
	        			MainpageActivity.databaseHelper.deleteAllTvSet();
	        		else
	        			Log.d("netlink","服务器没有新信息");
	        		for(int i = 0;i < replybody.getTvInfo().size(); i ++){
	        			TvSetInfo info = replybody.getTvInfo().get(i);
	        			MainpageActivity.databaseHelper.tryToInsertTvCat(info.cat, "", R.drawable.feng, null);
	        			Log.d("byrtv","+--"+info.name+"---"+info.desc+"---"+info.url+"--"+info.img+"---"+info.cat+"--+");
	        			MainpageActivity.databaseHelper.tryToInsertTvSet(info.name, info.desc, info.url, info.img, info.cat,null);
	        			
	        			/*if(MainpageActivity.databaseHelper != null){
	        				MainpageActivity.databaseHelper.tryToInsertRadioSet(info.name, info.desc, info.url, R.drawable.feng);
	        			}*/
	                }
	        		/*
	        		try{
        				Message msg = new Message();  
        	            msg.what = 2;  
        	            MainpageActivity.handler.sendMessage(msg);  
        			}catch(Exception e){
        				Log.d("netlink",e.toString());
        			}
        			*/
	        		Log.d("byrtv","收到服务器更新收音机频道信息"+replybody.getRadioInfo().size());
	        	}else if(replybody.REPLY_TYEP == ReplyBody.TYPE_AD){
	        		MainpageActivity.addUrl = replybody.adUrl;
	        		MainpageActivity.adContent = replybody.adContent;
	        		MainpageActivity.adIsVideo = replybody.adIsVideo;
	        		try{
        				Message msg = new Message();  
        	            msg.what = -10;  
        	            FirstFragment.handler.sendMessage(msg);  
        			}catch(Exception e){
        				Log.d("netlink","handler+  "+e.toString());
        				Thread.sleep(300);
        				ConnectionServer.requireAd();
        			}
	        		Log.d("netlink","收到服务器更新广告信息");
	        	}
	        }else if(message instanceof RecommendInfo){
	        	RecommendInfo rf = (RecommendInfo)message;
	        	if( rf.tvList.size() != 0)
	        		MainpageActivity.databaseHelper.deleteAllRecommend(null);
	        	for(int i = 0;i < rf.tvList.size(); i ++){
        			RecommendSetInfo info = rf.tvList.get(i);
        			Log.d("byrtv","+--"+info.name+"---"+info.desc+"---"+info.url+"--"+info.img+"---"+info.cat+"--+");
        			MainpageActivity.databaseHelper.tryToInsertRecommend(info.name, info.desc, info.url, info.img, info.cat,null);
                }
	        	try{
    				Message msg = new Message();  
    	            msg.what = 2;  
    	            MainpageActivity.handler.sendMessage(msg);  
    			}catch(Exception e){
    				Log.d("netlink",e.toString());
    			}
	        }
	    } 
	 
	    public void exceptionCaught(IoSession arg0, Throwable arg1) 
	            throws Exception { 
	 
	    }
}
