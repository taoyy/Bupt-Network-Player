package com.byr.network;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class RadioInfo implements Serializable{
	List<RadioSetInfo> radioList = null;
	public RadioInfo(ResultSet result) throws SQLException{
		radioList = new  ArrayList<RadioSetInfo>();
		while(result.next()) {
			String name = result.getString("name");
			String descri = result.getString("descri");
			String url = result.getString("url");
			radioList.add(new RadioSetInfo(name,descri,url));
		}
	}
	public class RadioSetInfo implements Serializable{
		String name;
		String desc;
		String url;
		public RadioSetInfo(String name,String desc,String url){
			this.name = name;
			this.desc = desc;
			this.url = url;
		}
	}
}