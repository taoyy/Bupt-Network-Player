package com.byr.network;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RecommendInfo implements Serializable{
	List<RecommendSetInfo> tvList = null;
	public RecommendInfo(ResultSet result) throws SQLException{
		tvList = new  ArrayList<RecommendSetInfo>();
		while(result.next()) {
			String name = result.getString("name");
			String descri = result.getString("descri");
			String url = result.getString("url");
			String img = result.getString("img");
			String cat = result.getString("cat");
			tvList.add(new RecommendSetInfo(name,descri,url,img,cat));
		}
	}
	public class RecommendSetInfo implements Serializable{
		String name;
		String desc;
		String url;
		String img;
		String cat;
		public RecommendSetInfo(String name,String desc,String url,String img,String cat){
			this.name = name;
			this.desc = desc;
			this.url = url;
			this.img = img;
			this.cat = cat;
		}
	}
}