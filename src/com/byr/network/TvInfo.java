package com.byr.network;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TvInfo implements Serializable{
	List<TvSetInfo> tvList = null;
	public TvInfo(ResultSet result) throws SQLException{
		tvList = new  ArrayList<TvSetInfo>();
		while(result.next()) {
			String name = result.getString("name");
			String descri = result.getString("descri");
			String url = result.getString("url");
			String img = result.getString("img");
			String cat = result.getString("cat");
			tvList.add(new TvSetInfo(name,descri,url,img,cat));
		}
	}
	public class TvSetInfo implements Serializable{
		String name;
		String desc;
		String url;
		String img;
		String cat;
		public TvSetInfo(String name,String desc,String url,String img,String cat){
			this.name = name;
			this.desc = desc;
			this.url = url;
			this.img = img;
			this.cat = cat;
		}
	}
}