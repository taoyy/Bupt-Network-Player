package com.byr.network;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.transport.socket.SocketConnector;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import android.util.Log;

public class ConnectionServer {
	static SocketConnector connector;
	static ConnectFuture future;
	public static void sendHello() {
		connector = new NioSocketConnector();
		IoFilter filter = new ProtocolCodecFilter(new ObjectSerializationCodecFactory());
		connector.getFilterChain().addLast("vestigge", filter);
		SocketAddress soketAddress = new InetSocketAddress("10.3.18.194", 5469);//主服务器
		//SocketAddress soketAddress = new InetSocketAddress("172.27.35.1", 5469);//寝室服务器
		//SocketAddress soketAddress = new InetSocketAddress("192.168.1.100", 5469);//测试用
		connector.setHandler(new ClientHandler());
		future = connector.connect(soketAddress);
		future.awaitUninterruptibly();
		if (!future.isConnected()) {
			Log.d("netlink","failed");
			System.out.println("连接服务器失败");
			return;
		}
		SendBody sendbody = new SendBody(SendBody.TYPE_HELLO);
		future.getSession().write(sendbody);
	}
	
	public static boolean requireRadioUpdate(){
		if(!connector.isActive()){
			sendHello();
		}
		try{
			future.getSession().write(new SendBody(SendBody.TYPE_GET_RADIO_UPDATE));
			return true;
		}catch(Exception e){
			Log.d("netlink","failed");
			return false;
		}
	}
	
	public static boolean requireTvUpdate(){
		if(!connector.isActive()){
			sendHello();
		}
		try{
			future.getSession().write(new SendBody(SendBody.TYPE_GET_TV_UPDATE));
			return true;
		}catch(Exception e){
			Log.d("netlink","failed");
			return false;
		}
	}
	
	public static boolean requireAd(){
		if(!connector.isActive()){
			sendHello();
		}
		try{
			future.getSession().write(new SendBody(SendBody.TYPE_GET_AD));
			return true;
		}catch(Exception e){
			Log.d("netlink","failed");
			return false;
		}
	}
	
	public static boolean requireRecommend(){
		if(!connector.isActive()){
			sendHello();
		}
		try{
			SendBody sb = new SendBody(SendBody.TYPE_GET_OTHER);
			sb.GET_OTHER_NAME = "GET_RECOMMEND";
			future.getSession().write(sb);
			return true;
		}catch(Exception e){
			Log.d("netlink","failed");
			return false;
		}
	}
    
}
