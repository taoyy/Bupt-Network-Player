package com.byr.tools;

public interface VoiceManageListener {
	public void OnPreparing();
	public void OnPrepared();
	public void OnError(Exception e);
	public void OnPause();
	public void OnReset();
	public void OnResume();
	public void OnComplete();
	public void OnPlay();
}
