package com.byr.tools;

public class ChatRecord {
	private String username;
	private String content;
	private boolean isSentBySelf;
	public ChatRecord(String username, String content, boolean isSentBySelf) {
		this.username = username;
		this.content = content;
		this.isSentBySelf = isSentBySelf;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public boolean isSentBySelf() {
		return isSentBySelf;
	}
	public void setSentBySelf(boolean isSentBySelf) {
		this.isSentBySelf = isSentBySelf;
	}
}
