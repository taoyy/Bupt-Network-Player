package com.byr.tools;

import java.util.ArrayList;

public class SafeArrayList<E> extends ArrayList<E>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public synchronized boolean add (E object) {
		return super.add(object);
	}
	@Override
	public synchronized void clear () {
		super.clear();
	}
}
