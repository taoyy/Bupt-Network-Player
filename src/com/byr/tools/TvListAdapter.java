package com.byr.tools;

import com.byr.byrplayer.R;
import com.byr.database.ByrPlayerDatabaseHelper;
import com.darrenmowat.imageloader.library.NetworkImageView;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class TvListAdapter extends CursorAdapter{
	Context mcontext;
	public TvListAdapter( Cursor c,Context context) {
		// TODO Auto-generated constructor stub
		super(context, c, true);
		mcontext = context;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// TODO Auto-generated method stub
		String name = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_NAME));
		String detial = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_DESCR));
		String url = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_URL));
		String cat = 	cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_CAT_NAME));
		String imgurl = 	cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_IMG_URL));
		
		TextView radioName = (TextView) view.findViewById(R.id.ItemTitle);
		TextView radioDetial = (TextView) view.findViewById(R.id.ItemText);
		TextView radioUrl = (TextView) view.findViewById(R.id.url);
		TextView radioCat = (TextView) view.findViewById(R.id.Itemcat);
		NetworkImageView logo = (NetworkImageView) view.findViewById(R.id.ItemImage);
		if(name != null)
			radioName.setText(name);
		if(detial != null )
			radioDetial.setText(detial);
		if(url != null )
			radioUrl.setText(url);
		if(cat != null)
			radioCat.setText(cat);
		if(imgurl != null && imgurl != "")
			logo.setImageURI(Uri.parse(imgurl));
		logo.setImageUrl(imgurl,(Activity)mcontext);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflate = LayoutInflater.from(context);
		View view = inflate.inflate(R.layout.layout_recommend, null);
		bindView(view, context, cursor);
		return view;
	}

}
