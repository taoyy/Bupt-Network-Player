package com.byr.tools;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceServer {
	public static void saveUserData(String username,Context context){
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		SharedPreferences.Editor editor = mySharedPreferences.edit(); 
		editor.putString("username",username); 
		editor.commit(); 
	}
	public static void removeUserData(Context context){
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		SharedPreferences.Editor editor = mySharedPreferences.edit(); 
		editor.putString("username",""); 
		editor.commit(); 
	}
	public static boolean hasData(Context context){
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		String name =mySharedPreferences.getString("username", "");
		if(!name.equals(""))
			return true;
		return false;	
	}
	public static String getUser(Context context){
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		String name =mySharedPreferences.getString("username", "").toLowerCase();//因为环信用户名必须是小写，没办法喽
		return name;
	}
	public static String getUpperUser(Context context){//要大写用户名的用这个
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		String name =mySharedPreferences.getString("username", "").toLowerCase();//因为环信用户名必须是小写，没办法喽
		return name;
	}
	public static void saveBBSUrl(String url,Context context){
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		SharedPreferences.Editor editor = mySharedPreferences.edit(); 
		editor.putString("bbsurl",url); 
		editor.commit(); 
	}
	public static boolean hasBBSUrl(Context context){
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		String name =mySharedPreferences.getString("bbsurl", "");
		if(!name.equals(""))
			return true;
		return false;	
	}
	public static String getBBSUrl(Context context){
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		String name =mySharedPreferences.getString("bbsurl", "").toLowerCase();//因为环信用户名必须是小写，没办法喽
		return name;
	}
	public static void setAutoLogin(boolean autologin,Context context){
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		SharedPreferences.Editor editor = mySharedPreferences.edit(); 
		if(autologin)
			editor.putString("autologin","true"); 
		else
			editor.putString("autologin",""); 
		editor.commit(); 
	}
	public static boolean isAutoLogin(Context context){
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		String name =mySharedPreferences.getString("autologin", "");
		if(name.equals("")){
			return false;
		}
		return true;
	}
	public static void setNotReceiveGroupmessage(boolean nogroupmess,Context context){
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		SharedPreferences.Editor editor = mySharedPreferences.edit(); 
		if(nogroupmess)
			editor.putString("nogroupmessage","true"); 
		else
			editor.putString("nogroupmessage",""); 
		editor.commit(); 
	}
	public static boolean isNotReceiveGroupmessage(Context context){
		SharedPreferences mySharedPreferences = context.getSharedPreferences("userdata", Activity.MODE_PRIVATE); 
		String name =mySharedPreferences.getString("nogroupmessage", "");
		if(name.equals("")){
			return false;
		}
		return true;
	}
}
