package com.byr.tools;

import com.byr.byrplayer.R;
import com.byr.database.ByrPlayerDatabaseHelper;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class RadioAdapter extends CursorAdapter {

	public RadioAdapter(Context context, Cursor c, boolean autoRequery) {
		super(context, c, false);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// TODO Auto-generated method stub
		String name = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.RADIO_SET_NAME));
		String detial = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.RADIO_SET_DESCR));
		String url = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.RADIO_SET_URL));
		//int img = 	cursor.getInt(cursor.getColumnIndex(ByrPlayerDatabaseHelper.RADIO_SET_IMG_ID));
		
		TextView radioName = (TextView) view.findViewById(R.id.ItemTitle);
		TextView radioDetial = (TextView) view.findViewById(R.id.ItemText);
		TextView radioUrl = (TextView) view.findViewById(R.id.url);
		
		if(name != null)
			radioName.setText(name);
		if(detial != null )
			radioDetial.setText(detial);
		if(url != null )
			radioUrl.setText(url);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflate = LayoutInflater.from(context);
		View view = inflate.inflate(R.layout.layout_items, null);
		bindView(view, context, cursor);
		return view;
	}

}
