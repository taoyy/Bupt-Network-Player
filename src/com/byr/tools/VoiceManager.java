package com.byr.tools;

import android.util.Log;

import com.byr.byrplayer.MainpageActivity;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.MediaPlayer.OnCompletionListener;
import io.vov.vitamio.MediaPlayer.OnErrorListener;
import io.vov.vitamio.MediaPlayer.OnPreparedListener;
/**
 * 
 * @author icybee
 * @version 1.0.0
 * 这个类运行在单例模式下
 */
public class VoiceManager {
	private static VoiceManager instance = null;
	private MediaPlayer mediaplay;
	public static int STATE_INITED = 0;
	public static int STATE_LOADED = 1;
	public static int STATE_PREPARING = 2;
	public static int STATE_PAUSED = 3;
	public static int STATE_PLAYING = 4;
	
	
	VoiceManageListener voiceManageListener;
	
	private int state;
	private VoiceManager(){
		//mediaplay = new MediaPlayer(MainpageActivity.mcontext);
		state = STATE_INITED;
		voiceManageListener = null;
	}
	/**
	 * @author icybee
	 * @return the single instance of this class
	 */
	public static VoiceManager getInstance(){
		if(instance == null){
			instance = new VoiceManager();
		}
		return instance;
	}
	
	public boolean playVoiceAsync(final String url){
		new Thread(){
			public void run(){
				playVoice(url);
			}
		}.start();
		return true;
	}
	
	private boolean playVoice(String url){
		if(mediaplay != null && mediaplay.isPlaying()){
			mediaplay.stop();
			mediaplay.release();
		}
		mediaplay = new MediaPlayer(MainpageActivity.mcontext);//directly abandon the old media played(if exist)
		mediaplay.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				if(voiceManageListener != null)
					voiceManageListener.OnComplete();
			}
		});
		state = STATE_PREPARING;
		
		if(voiceManageListener != null)
			voiceManageListener.OnPreparing();
		try {
			mediaplay.setDataSource(url);
			Log.d("byrmediaplay","setDataSource - " + url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(voiceManageListener != null)
				voiceManageListener.OnError(e);
			reset();
			return false;
		} 
		state = STATE_LOADED;
		mediaplay.setOnErrorListener(new OnErrorListener(){

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				// TODO Auto-generated method stub
				Log.d("byrmediaplay","playVoice - OnError" + what + extra);
				return false;
			}
			
		});
		Log.d("byrmediaplay","playVoice - pre - prepareAsync");
		mediaplay.prepareAsync();
		Log.d("byrmediaplay","playVoice - after - prepareAsync");
		mediaplay.setOnPreparedListener(new OnPreparedListener(){
			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				onMediaPrepared();
			}
			
		});
		return true;
	}
	
	private void onMediaPrepared(){
		if(mediaplay != null && !mediaplay.isPlaying()){
			mediaplay.start();
			state = STATE_PLAYING;
			if(voiceManageListener != null)
				voiceManageListener.OnPrepared();
				voiceManageListener.OnPlay();
		}
	}
	
	public void pauseVoice(){
		if(mediaplay != null && mediaplay.isPlaying()){
			mediaplay.pause();
			state = STATE_PAUSED;
			if(voiceManageListener != null)
				voiceManageListener.OnPause();
		}
	}
	
	public int getState(){
		return this.state;
	}
	
	public void setVoiceManageListener(VoiceManageListener listener){
		voiceManageListener = listener;
	}
	
	public void reset(){
		mediaplay = null;
		state = STATE_INITED;
		if(voiceManageListener != null)
			voiceManageListener.OnReset();
	}
	
	public void resume(){
		if(mediaplay != null && state == STATE_PAUSED){
			mediaplay.start();
			state = STATE_PLAYING;
			if(voiceManageListener != null)
				voiceManageListener.OnResume();
		}
	}
	
	/**
	 * 在程序退出时调用
	 */
	public void destroy(){
		if(mediaplay != null){
			mediaplay.stop();
			mediaplay.release();
		}
		mediaplay = null;
	}
}
