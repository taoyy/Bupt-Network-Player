package com.byr.tools;

import java.util.ArrayList;

public class ChatRecordKeeper {
	private static SafeArrayList<ChatRecord> List = new SafeArrayList<ChatRecord>(); 
	public static void addRecord(String username,String content,boolean isSentBySelf){
		List.add(new ChatRecord(username, content, isSentBySelf));
	}
	public static void clear(){
		List.clear();
	}
	public static SafeArrayList<ChatRecord> getList() {
		return List;
	}

}
