package com.byr.bbsqauth;

public interface Constants {
	/** 填写自己的 APP_KEY */
	public static final String APP_KEY = "a6dc339725dc9561d3978e0c4a1934b1";
    /** 
     * 应用的回调页。
     * 建议使用默认回调页：http://bbs.byr.cn/Oauth2/callback
     */
    public static final String REDIRECT_URL = "http://eid.byr.cn/paper/nforum/oauth2/callback";
    /**
     * 应用对应的权限，设置成空即获取用户的详细信息。
     * 多个项目请用逗号隔开（不要有空格！！！）
     */
    public static final String SCOPE = "article,mail,favor,refer,blacklist";// "article,mail,favor,refer,blacklis";


}
